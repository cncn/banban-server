Introduction
==============================================

伴伴服务器系统，为其他系统提供数据读写服务，相关的接口以HTTP方式暴露，除此之外，还提供数据对象等开发组件。

* 数据访问服务
* 支持高并发访问
* 支持缓存(暂不开发)
* 提供开发组件

### Requirements
Jdk 1.7，MySQL 5

### External Deps

* Netty (http://netty.io): 由JBOSS提供的一个java开源框架，提供异步的、事件驱动的网络应用程序框架和工具，用以快速开发高性能、高可靠性的网络服务器和客户端程序。
* Gson (http://code.google.com/p/google-gson): Google公司发布的一个开放源代码的Java库，主要用途为序列化Java对象为JSON字符串，或反序列化JSON字符串成Java对象。

### Standard Libary Deps

* XML-RPC: xml-rpc调用

### Installation

* 安装Java和JBoss

### Tests

* 测试框架：junit
* 持续集成服务器: null
* 持续集成服务：null
* MySQL Server: 121.40.212.164:3306  dbhuafang2014
* 文件规划介绍：null

### More Information

* 设计文档：null
* 同步设计文档：null

### Example Usage

*some usage examples will be add here.

### Issue Tracking

* some issues

### Support

* 马旋(开发工程师)： { "昵称": "驰骋", "email": "mxyydy126@gmail.com" }
