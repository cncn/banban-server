package com.banban.client;

import com.banban.common.ResultOfDataAccess;
import com.banban.common.dataobjects.*;
import com.banban.common.request.Command;
import com.banban.common.utils.ByteBufToBytes;
import com.google.gson.Gson;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.codec.http.HttpContent;
import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.codec.http.HttpResponse;

import java.util.List;

/**
 * Created by Administrator on 2014/11/29.
 */

public class HttpClientInboundHandler extends ChannelInboundHandlerAdapter {
    private ByteBufToBytes reader;

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        if (msg instanceof HttpResponse) {
            HttpResponse response = (HttpResponse) msg;
            System.out.println("\nCONTENT_TYPE:" + response.headers().get(HttpHeaders.Names.CONTENT_TYPE));
            if (HttpHeaders.isContentLengthSet(response)) {
                reader = new ByteBufToBytes((int) HttpHeaders.getContentLength(response));
            }
        }

        if (msg instanceof HttpContent) {
            HttpContent httpContent = (HttpContent) msg;
            ByteBuf content = httpContent.content();
            reader.reading(content);
            content.release();

            if (reader.isEnd()) {
                String resultStr = new String(reader.readFull());
                System.out.println("Server said:" + resultStr);
                ResultOfDataAccess resultOfDataAccess = new Gson().fromJson(resultStr,ResultOfDataAccess.class);
                if(resultOfDataAccess.isSuccess()) {
                    if(resultOfDataAccess.getCommandInRequest() == Command.RegisterUserByDataObject) {
                        String jsonResult = new Gson().toJson(resultOfDataAccess.getResult());
                        Long newUserId = new Gson().fromJson(jsonResult, Long.class);
                        System.out.println("The new user's id is " + newUserId + ".");
                    } else if(resultOfDataAccess.getCommandInRequest() == Command.DisableUserByLoginIdAndPassword) {
                        String jsonResult = new Gson().toJson(resultOfDataAccess.getResult());
                        Integer affectedRowNum = new Gson().fromJson(jsonResult, Integer.class);
                        System.out.println("The operation of disabling user is " +
                                (affectedRowNum == 1 ? "success":"failed") + ".");
                    } else if(resultOfDataAccess.getCommandInRequest() == Command.UpdateUserByDataObject) {
                        String jsonResult = new Gson().toJson(resultOfDataAccess.getResult());
                        Integer affectedRowNum = new Gson().fromJson(jsonResult, Integer.class);
                        System.out.println("The operation of updating user is " +
                                (affectedRowNum == 1 ? "success":"failed") + ".");
                    } else if(resultOfDataAccess.getCommandInRequest() == Command.GetUserByLoginIdAndPassword) {
                        String jsonResult = new Gson().toJson(resultOfDataAccess.getResult());
                        UserDO userDO = new Gson().fromJson(jsonResult, UserDO.class);
                        System.out.println("The user's nickname and birthday is " + userDO.getNickName() + " and "
                                + userDO.getBirthday() + ".");
                    } else if(resultOfDataAccess.getCommandInRequest() == Command.VisitUserById) {
                        String jsonResult = new Gson().toJson(resultOfDataAccess.getResult());
                        UserDO userDO = new Gson().fromJson(jsonResult, UserDO.class);
                        System.out.println("The user's nickname and password is " + userDO.getNickName() + " and "
                                + userDO.getPassword() + ".");
                    }

                    else if(resultOfDataAccess.getCommandInRequest() == Command.RegisterPsychologistByDataObject) {
                        String jsonResult = new Gson().toJson(resultOfDataAccess.getResult());
                        Long newPsychologistId = new Gson().fromJson(jsonResult, Long.class);
                        System.out.println("The new psychologist's id is " + newPsychologistId + ".");
                    } else if(resultOfDataAccess.getCommandInRequest() == Command.DisablePsychologistByLoginIdAndPassword) {
                        String jsonResult = new Gson().toJson(resultOfDataAccess.getResult());
                        Integer affectedRowNum = new Gson().fromJson(jsonResult, Integer.class);
                        System.out.println("The operation of disabling psychologist is " +
                                (affectedRowNum == 1 ? "success":"failed") + ".");
                    } else if(resultOfDataAccess.getCommandInRequest() == Command.UpdatePsychologistByDataObject) {
                        String jsonResult = new Gson().toJson(resultOfDataAccess.getResult());
                        Integer affectedRowNum = new Gson().fromJson(jsonResult, Integer.class);
                        System.out.println("The operation of updating psychologist is " +
                                (affectedRowNum == 1 ? "success":"failed") + ".");
                    } else if(resultOfDataAccess.getCommandInRequest() == Command.GetPsychologistByLoginIdAndPassword) {
                        String jsonResult = new Gson().toJson(resultOfDataAccess.getResult());
                        PsychologistDO psychologistDO = new Gson().fromJson(jsonResult, PsychologistDO.class);
                        System.out.println("The psychologist's nickname and idCardNo is " + psychologistDO.getNickName() + " and "
                                + psychologistDO.getIdCardNo() + ".");
                    } else if(resultOfDataAccess.getCommandInRequest() == Command.VisitPsychologistById) {
                        String jsonResult = new Gson().toJson(resultOfDataAccess.getResult());
                        PsychologistDO psychologistDO = new Gson().fromJson(jsonResult, PsychologistDO.class);
                        System.out.println("The psychologist's nickname, password and idCardNo is " +
                                psychologistDO.getNickName() + ", " + psychologistDO.getPassword() + " and " +
                                psychologistDO.getIdCardNo() + ".");
                    } else if(resultOfDataAccess.getCommandInRequest() == Command.GetRecommendedPsychologists) {
                        String jsonResult = new Gson().toJson(resultOfDataAccess.getResult());
                        List psychologists = new Gson().fromJson(jsonResult, List.class);
                        System.out.println("There are " + psychologists.size() + " recommended psychologist.");
                        if(psychologists.size() > 0) {
                            String psychologistDOString = new Gson().toJson(psychologists.get(0));
                            PsychologistDO psychologistDO = new Gson().fromJson(psychologistDOString, PsychologistDO.class);
                            System.out.println("And one's nickname is " + psychologistDO.getNickName() + ".");
                        }
                    }

                    else if(resultOfDataAccess.getCommandInRequest() == Command.RegisterRelationshipByDataObject) {
                        String jsonResult = new Gson().toJson(resultOfDataAccess.getResult());
                        Long newRelationshipId = new Gson().fromJson(jsonResult, Long.class);
                        System.out.println("The new relationship's id is " + newRelationshipId + ".");
                    } else if(resultOfDataAccess.getCommandInRequest() == Command.UpdateRelationshipByDataObject) {
                        String jsonResult = new Gson().toJson(resultOfDataAccess.getResult());
                        Integer affectedRowNum = new Gson().fromJson(jsonResult, Integer.class);
                        System.out.println("The operation of updating relationship is " +
                                (affectedRowNum == 1 ? "success":"failed") + ".");
                    } else if(resultOfDataAccess.getCommandInRequest() == Command.GetAllFriendsByUserId) {
                        String jsonResult = new Gson().toJson(resultOfDataAccess.getResult());
                        List friends = new Gson().fromJson(jsonResult, List.class);
                        if(friends.size() > 0) {
                            String psychologistDOString = new Gson().toJson(friends.get(0));
                            PsychologistDO psychologistDO = new Gson().fromJson(psychologistDOString, PsychologistDO.class);
                            System.out.println("The nickname of the first psychologist is " + psychologistDO.getNickName() + ".");
                        }
                    } else if(resultOfDataAccess.getCommandInRequest() == Command.GetAllFriendsByPsyUid) {
                        String jsonResult = new Gson().toJson(resultOfDataAccess.getResult());
                        List friends = new Gson().fromJson(jsonResult, List.class);
                        if(friends.size() > 0) {
                            String userDOString = new Gson().toJson(friends.get(0));
                            UserDO userDO = new Gson().fromJson(userDOString, UserDO.class);
                            System.out.println("The nickname of the first user is " + userDO.getNickName() + ".");
                        }
                    }

                    else if(resultOfDataAccess.getCommandInRequest() == Command.GetPhotosByUserId) {
                        String jsonResult = new Gson().toJson(resultOfDataAccess.getResult());
                        List photos = new Gson().fromJson(jsonResult, List.class);
                        System.out.println("There are " + photos.size() + " photos.");
                        if(photos.size() > 0) {
                            String photoDOString = new Gson().toJson(photos.get(0));
                            PhotoDO photoDO = new Gson().fromJson(photoDOString, PhotoDO.class);
                            System.out.println("And the remark of one photo is " + photoDO.getRemark() + ".");
                        }
                    }

                    else if(resultOfDataAccess.getCommandInRequest() == Command.BindTagByDataObject) {
                        String jsonResult = new Gson().toJson(resultOfDataAccess.getResult());
                        Long newTagId = new Gson().fromJson(jsonResult, Long.class);
                        System.out.println("The new tag's id is " + newTagId + ".");
                    } else if(resultOfDataAccess.getCommandInRequest() == Command.UnBindTagByDataObject) {
                        String jsonResult = new Gson().toJson(resultOfDataAccess.getResult());
                        Integer affectedRowNum = new Gson().fromJson(jsonResult, Integer.class);
                        System.out.println("The operation of unbinding tag is " +
                                (affectedRowNum == 1 ? "success":"failed") + ".");
                    } else if(resultOfDataAccess.getCommandInRequest() == Command.GetTagsByPsyUId) {
                        String jsonResult = new Gson().toJson(resultOfDataAccess.getResult());
                        List tagList = new Gson().fromJson(jsonResult, List.class);
                        if(tagList.size() > 0) {
                            String tagDOString = new Gson().toJson(tagList.get(0));
                            TagDO tagDO = new Gson().fromJson(tagDOString, TagDO.class);
                            System.out.println("The first tag of the user is " + tagDO.getTag() + ".");
                        }
                    }

                    else if(resultOfDataAccess.getCommandInRequest() == Command.PostDemandOrderByDataObject) {
                        String jsonResult = new Gson().toJson(resultOfDataAccess.getResult());
                        Long newDemandOrderId = new Gson().fromJson(jsonResult, Long.class);
                        System.out.println("The new demand order's id is " + newDemandOrderId + ".");
                    } else if(resultOfDataAccess.getCommandInRequest() == Command.CancelDemandOrderById) {
                        String jsonResult = new Gson().toJson(resultOfDataAccess.getResult());
                        Integer affectedRowNum = new Gson().fromJson(jsonResult, Integer.class);
                        System.out.println("The operation of canceling demand order is " +
                                (affectedRowNum == 1 ? "success":"failed") + ".");
                    } else if(resultOfDataAccess.getCommandInRequest() == Command.UpdateDemandOrderByDataObject) {
                        String jsonResult = new Gson().toJson(resultOfDataAccess.getResult());
                        Integer affectedRowNum = new Gson().fromJson(jsonResult, Integer.class);
                        System.out.println("The operation of updating demand order is " +
                                (affectedRowNum == 1 ? "success":"failed") + ".");
                    }

                    else if(resultOfDataAccess.getCommandInRequest() == Command.AcceptDealOrderThroughDemandOrder) {
                        String jsonResult = new Gson().toJson(resultOfDataAccess.getResult());
                        Long newDealOrderId = new Gson().fromJson(jsonResult, Long.class);
                        System.out.println("The new deal order's id is " + newDealOrderId + ".");
                    } else if(resultOfDataAccess.getCommandInRequest() == Command.UpdateDealOrderByDataObject) {
                        String jsonResult = new Gson().toJson(resultOfDataAccess.getResult());
                        Integer affectedRowNum = new Gson().fromJson(jsonResult, Integer.class);
                        System.out.println("The operation of updating deal order is " +
                                (affectedRowNum == 1 ? "success":"failed") + ".");
                    } else if(resultOfDataAccess.getCommandInRequest() == Command.GetDealOrdersByUserId) {
                        String jsonResult = new Gson().toJson(resultOfDataAccess.getResult());
                        List dealOrderList = new Gson().fromJson(jsonResult, List.class);
                        if(dealOrderList.size() > 0) {
                            String dealOrderDOString = new Gson().toJson(dealOrderList.get(0));
                            DealOrderDO dealOrderDO = new Gson().fromJson(dealOrderDOString, DealOrderDO.class);
                            System.out.println("The service time of first deal order is " + dealOrderDO.getServiceTime() + " hours.");
                        }
                    } else if(resultOfDataAccess.getCommandInRequest() == Command.GetDealOrdersByPsyUId) {
                        String jsonResult = new Gson().toJson(resultOfDataAccess.getResult());
                        List dealOrderList = new Gson().fromJson(jsonResult, List.class);
                        if(dealOrderList.size() > 0) {
                            String dealOrderDOString = new Gson().toJson(dealOrderList.get(0));
                            DealOrderDO dealOrderDO = new Gson().fromJson(dealOrderDOString, DealOrderDO.class);
                            System.out.println("The service time of first deal order is " + dealOrderDO.getServiceTime() + " hours.");
                        }
                    }
                } else {
                    String jsonResult = new Gson().toJson(resultOfDataAccess.getResult());
                    System.out.println("Data access failed! Message from server: " + jsonResult + ".");
                }
                ctx.close();
            }
        }
    }
}