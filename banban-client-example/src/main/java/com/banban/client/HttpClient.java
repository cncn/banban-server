package com.banban.client;

import com.banban.common.RequestOfDataAccess;
import com.banban.common.dataobjects.*;
import com.banban.common.request.Command;
import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Random;

/**
 * Created by Administrator on 2015/2/5.
 */
public class HttpClient {
    private static HttpClientSender httpClientSender = new HttpClientSender();

    private static void RegisterUserByDataObject() throws Exception {
        RequestOfDataAccess requestOfDataAccess = new RequestOfDataAccess();
        requestOfDataAccess.setCommand(Command.RegisterUserByDataObject);
        UserDO newUserDO = new UserDO();
        Random random = new Random();
        int randomLength = random.nextInt(4) + 6;
        String loginId = "";
        String nickName = "";
        for(int i = 0; i < randomLength; ++i) {
            loginId += (char) (random.nextInt(26) + 97);
            nickName += (char) (random.nextInt(26) + 97);
        }
        String subCellPhone = "";
        for(int i = 0; i < 4; ++i) subCellPhone += (char) (random.nextInt(10) + 48);
        char month  = (char) (random.nextInt(9) + 49);
        Date birthday = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse("1990-0" + month + "-01 00:00:00");
        newUserDO.setLoginId(loginId);
        newUserDO.setPassword("123456");
        newUserDO.setCellphone("1381704" + subCellPhone);
        newUserDO.setNickName(nickName);
        newUserDO.setGender(random.nextBoolean() ? "GG" : "MM");
        newUserDO.setBirthday(birthday);
        requestOfDataAccess.setArgumentList(newUserDO);
        String requestMsg = new Gson().toJson(requestOfDataAccess);
        httpClientSender.sendRequest(requestMsg);
    }

    private static void DisableUserByLoginIdAndPassword() throws Exception{
        RequestOfDataAccess requestOfDataAccess = new RequestOfDataAccess();
        requestOfDataAccess.setCommand(Command.DisableUserByLoginIdAndPassword);
        HashMap<String, String> loginIdAndPassword = new HashMap<String, String>();
        loginIdAndPassword.put("loginId", "mxyydy");
        loginIdAndPassword.put("password", "123456");
        requestOfDataAccess.setArgumentList(loginIdAndPassword);
        String requestMsg = new Gson().toJson(requestOfDataAccess);
        httpClientSender.sendRequest(requestMsg);
    }

    private static void UpdateUserByDataObject() throws Exception {
        RequestOfDataAccess requestOfDataAccess = new RequestOfDataAccess();
        requestOfDataAccess.setCommand(Command.UpdateUserByDataObject);
        UserDO updatedUserDO = new UserDO();
        updatedUserDO.setId(2L);
        Random random = new Random();
        int randomLength = random.nextInt(4) + 6;
        String email = "";
        for(int i = 0; i < randomLength; ++i) email += (char) (random.nextInt(26) + 97);
        updatedUserDO.setEmail(email + "@gmail.com");
        updatedUserDO.setStatus("Y");
        requestOfDataAccess.setArgumentList(updatedUserDO);
        String requestMsg = new Gson().toJson(requestOfDataAccess);
        httpClientSender.sendRequest(requestMsg);
    }

    private static void GetUserByLoginIdAndPassword() throws Exception {
        RequestOfDataAccess requestOfDataAccess = new RequestOfDataAccess();
        requestOfDataAccess.setCommand(Command.GetUserByLoginIdAndPassword);
        HashMap<String, String> loginIdAndPassword = new HashMap<String, String>();
        loginIdAndPassword.put("loginId", "mxyydy");
        loginIdAndPassword.put("password", "123456");
        requestOfDataAccess.setArgumentList(loginIdAndPassword);
        String requestMsg = new Gson().toJson(requestOfDataAccess);
        httpClientSender.sendRequest(requestMsg);
    }

    private static void VisitUserById() throws Exception {
        RequestOfDataAccess requestOfDataAccess = new RequestOfDataAccess();
        requestOfDataAccess.setCommand(Command.VisitUserById);
        requestOfDataAccess.setArgumentList(2L);
        String requestMsg = new Gson().toJson(requestOfDataAccess);
        httpClientSender.sendRequest(requestMsg);
    }

    private static void RegisterPsychologistByDataObject() throws Exception {
        RequestOfDataAccess requestOfDataAccess = new RequestOfDataAccess();
        requestOfDataAccess.setCommand(Command.RegisterPsychologistByDataObject);
        PsychologistDO newPsychologistDO = new PsychologistDO();
        Random random = new Random();
        int randomLength = random.nextInt(4) + 6;
        String loginId = "";
        String nickName = "";
        for(int i = 0; i < randomLength; ++i) {
            loginId += (char) (random.nextInt(26) + 97);
            nickName += (char) (random.nextInt(26) + 97);
        }
        String subCellPhone = "";
        for(int i = 0; i < 4; ++i) subCellPhone += (char) (random.nextInt(10) + 48);
        char month  = (char) (random.nextInt(9) + 49);
        Date birthday = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse("1990-0" + month + "-01 00:00:00");
        newPsychologistDO.setLoginId(loginId);
        newPsychologistDO.setPassword("123456");
        newPsychologistDO.setCellphone("1381704" + subCellPhone);
        newPsychologistDO.setNickName(nickName);
        newPsychologistDO.setGender("MM");
        newPsychologistDO.setBirthday(birthday);

        String is_in_service;
        String qualification = random.nextBoolean() ? "Y":"N";
        if(qualification.equals("N")) is_in_service = "N";
        else is_in_service = new Random().nextBoolean() ? "Y":"N";
        newPsychologistDO.setIsInService(is_in_service);
        newPsychologistDO.setQualification(qualification);
        String name = "";
        String[] words = {"向", "雪", "贾", "萱", "李", "晓", "白", "梦", "琳", "葱"};
        int lengthOfName = random.nextInt(3) + 2;
        for(int i = 0; i < lengthOfName; ++i) name += words[random.nextInt(10)];
        newPsychologistDO.setName(name);
        String idCardNo = "";
        for(int i = 0; i < 18; ++i) idCardNo += (char) (random.nextInt(10) + 48);
        newPsychologistDO.setIdCardNo(idCardNo);
        if(qualification.equals("Y")) {
            String idCardPath = "./id_card_";
            for(int i = 0; i < 5; ++i) idCardPath += (char) (random.nextInt(10) + 48);
            idCardPath += ".jpg";
            newPsychologistDO.setIdCardPath(idCardPath);
            newPsychologistDO.setLevel(random.nextInt(5) + 1);
            newPsychologistDO.setCustomerLimit(random.nextInt(5) + 5);
        }
        requestOfDataAccess.setArgumentList(newPsychologistDO);
        String requestMsg = new Gson().toJson(requestOfDataAccess);
        httpClientSender.sendRequest(requestMsg);
    }

    private static void DisablePsychologistByLoginIdAndPassword() throws Exception {
        RequestOfDataAccess requestOfDataAccess = new RequestOfDataAccess();
        requestOfDataAccess.setCommand(Command.DisablePsychologistByLoginIdAndPassword);
        HashMap<String, String> loginIdAndPassword = new HashMap<String, String>();
        loginIdAndPassword.put("loginId", "leonili");
        loginIdAndPassword.put("password", "123456");
        requestOfDataAccess.setArgumentList(loginIdAndPassword);
        String requestMsg = new Gson().toJson(requestOfDataAccess);
        httpClientSender.sendRequest(requestMsg);
    }

    private static void UpdatePsychologistByDataObject() throws Exception {
        RequestOfDataAccess requestOfDataAccess = new RequestOfDataAccess();
        requestOfDataAccess.setCommand(Command.UpdatePsychologistByDataObject);
        PsychologistDO updatedPsychologistDO = new PsychologistDO();
        updatedPsychologistDO.setPsyId(1L);
        updatedPsychologistDO.setIsInService("Y");
        updatedPsychologistDO.setQualification("Y");
        requestOfDataAccess.setArgumentList(updatedPsychologistDO);
        String requestMsg = new Gson().toJson(requestOfDataAccess);
        httpClientSender.sendRequest(requestMsg);
    }

    private static void GetPsychologistByLoginIdAndPassword() throws Exception {
        RequestOfDataAccess requestOfDataAccess = new RequestOfDataAccess();
        requestOfDataAccess.setCommand(Command.GetPsychologistByLoginIdAndPassword);
        HashMap<String, String> loginIdAndPassword = new HashMap<String, String>();
        loginIdAndPassword.put("loginId", "leonili");
        loginIdAndPassword.put("password", "123456");
        requestOfDataAccess.setArgumentList(loginIdAndPassword);
        String requestMsg = new Gson().toJson(requestOfDataAccess);
        httpClientSender.sendRequest(requestMsg);
    }

    private static void VisitPsychologistById() throws Exception {
        RequestOfDataAccess requestOfDataAccess = new RequestOfDataAccess();
        requestOfDataAccess.setCommand(Command.VisitPsychologistById);
        requestOfDataAccess.setArgumentList(1L);
        String requestMsg = new Gson().toJson(requestOfDataAccess);
        httpClientSender.sendRequest(requestMsg);
    }

    private static void GetRecommendedPsychologists() throws Exception {
        RequestOfDataAccess requestOfDataAccess = new RequestOfDataAccess();
        requestOfDataAccess.setCommand(Command.GetRecommendedPsychologists);
        String requestMsg = new Gson().toJson(requestOfDataAccess);
        httpClientSender.sendRequest(requestMsg);
    }

    private static void RegisterRelationshipByDataObject() throws Exception {
        RelationshipDO newRelationshipDO = new RelationshipDO();
        newRelationshipDO.setUserId(2L);
        newRelationshipDO.setPsyUid(1L);
        newRelationshipDO.setType(1);
        RequestOfDataAccess requestOfDataAccess = new RequestOfDataAccess();
        requestOfDataAccess.setCommand(Command.RegisterRelationshipByDataObject);
        requestOfDataAccess.setArgumentList(newRelationshipDO);
        String requestMsg = new Gson().toJson(requestOfDataAccess);
        httpClientSender.sendRequest(requestMsg);
    }

    private static void UpdateRelationshipByDataObject() throws Exception {
        RelationshipDO relationshipDO = new RelationshipDO();
        relationshipDO.setUserId(4L);
        relationshipDO.setPsyUid(5L);
        relationshipDO.setType(new Random().nextBoolean() ? 1 : 2);
        RequestOfDataAccess requestOfDataAccess = new RequestOfDataAccess();
        requestOfDataAccess.setCommand(Command.UpdateRelationshipByDataObject);
        requestOfDataAccess.setArgumentList(relationshipDO);
        String requestMsg = new Gson().toJson(requestOfDataAccess);
        httpClientSender.sendRequest(requestMsg);
    }

    private static void GetAllFriendsByUserId() throws Exception {
        RequestOfDataAccess requestOfDataAccess = new RequestOfDataAccess();
        requestOfDataAccess.setCommand(Command.GetAllFriendsByUserId);
        requestOfDataAccess.setArgumentList(2L);
        String requestMsg = new Gson().toJson(requestOfDataAccess);
        httpClientSender.sendRequest(requestMsg);
    }

    private static void GetAllFriendsByPsyUid() throws Exception {
        RequestOfDataAccess requestOfDataAccess = new RequestOfDataAccess();
        requestOfDataAccess.setCommand(Command.GetAllFriendsByPsyUid);
        requestOfDataAccess.setArgumentList(1L);
        String requestMsg = new Gson().toJson(requestOfDataAccess);
        httpClientSender.sendRequest(requestMsg);
    }

    private static void GetPhotosByUserId() throws Exception {
        RequestOfDataAccess requestOfDataAccess = new RequestOfDataAccess();
        requestOfDataAccess.setCommand(Command.GetPhotosByUserId);
        requestOfDataAccess.setArgumentList(1L);
        String requestMsg = new Gson().toJson(requestOfDataAccess);
        httpClientSender.sendRequest(requestMsg);
    }

    private static void BindTagByDataObject() throws Exception {
        RequestOfDataAccess requestOfDataAccess = new RequestOfDataAccess();
        requestOfDataAccess.setCommand(Command.BindTagByDataObject);
        TagDO newTagDO = new TagDO();
        Random random = new Random();
        Long[] psyUids = {1L, 5L};
        newTagDO.setPsyUid(psyUids[random.nextInt(psyUids.length)]);
        newTagDO.setTag(TagDO.Tags[random.nextInt(TagDO.Tags.length)]);
        requestOfDataAccess.setArgumentList(newTagDO);
        String requestMsg = new Gson().toJson(requestOfDataAccess);
        httpClientSender.sendRequest(requestMsg);
    }

    private static void UnBindTagByDataObject() throws Exception {
        RequestOfDataAccess requestOfDataAccess = new RequestOfDataAccess();
        requestOfDataAccess.setCommand(Command.UnBindTagByDataObject);
        TagDO newTagDO = new TagDO();
        Random random = new Random();
        Long[] psyUids = {1L, 5L};
        newTagDO.setPsyUid(psyUids[random.nextInt(psyUids.length)]);
        newTagDO.setTag(TagDO.Tags[random.nextInt(TagDO.Tags.length)]);
        requestOfDataAccess.setArgumentList(newTagDO);
        String requestMsg = new Gson().toJson(requestOfDataAccess);
        httpClientSender.sendRequest(requestMsg);
    }

    private static void GetTagsByPsyUId() throws Exception {
        RequestOfDataAccess requestOfDataAccess = new RequestOfDataAccess();
        requestOfDataAccess.setCommand(Command.GetTagsByPsyUId);
        requestOfDataAccess.setArgumentList(1L);
        String requestMsg = new Gson().toJson(requestOfDataAccess);
        httpClientSender.sendRequest(requestMsg);
    }

    private static void PostDemandOrderByDataObject() throws Exception {
        RequestOfDataAccess requestOfDataAccess = new RequestOfDataAccess();
        requestOfDataAccess.setCommand(Command.PostDemandOrderByDataObject);
        DemandOrderDO newPostOrderDO = new DemandOrderDO();
        Random random = new Random();
        int randomLength = random.nextInt(18) + 6;
        String demandRemark = "";
        for(int i = 0; i < randomLength; ++i) {
            char c = (char) (random.nextInt(26) + 97);
            demandRemark += c;
        }
        Long[] availableUserId = {3L, 4L};
        float[] availableTimeLength = {0.5f, 1.0f, 1.5f, 2.0f, 2.5f, 3.0f};
        String[] tags = {"萝莉","御姐","温柔","豪爽","乐观"};
        newPostOrderDO.setUserId(availableUserId[random.nextInt(2)]);
        newPostOrderDO.setDemandGender("MM");
        newPostOrderDO.setDemandTag(tags[random.nextInt(5)]);
        newPostOrderDO.setDemandRemark(demandRemark);
        newPostOrderDO.setDemandTime(availableTimeLength[random.nextInt(6)]);
        newPostOrderDO.setStatus("N");
        requestOfDataAccess.setArgumentList(newPostOrderDO);
        String requestMsg = new Gson().toJson(requestOfDataAccess);
        httpClientSender.sendRequest(requestMsg);
    }

    private static void CancelDemandOrderById() throws Exception {
        RequestOfDataAccess requestOfDataAccess = new RequestOfDataAccess();
        requestOfDataAccess.setCommand(Command.CancelDemandOrderById);
        requestOfDataAccess.setArgumentList(1L);
        String requestMsg = new Gson().toJson(requestOfDataAccess);
        httpClientSender.sendRequest(requestMsg);
    }

    private static void UpdateDemandOrderByDataObject() throws Exception {
        RequestOfDataAccess requestOfDataAccess = new RequestOfDataAccess();
        requestOfDataAccess.setCommand(Command.UpdateDemandOrderByDataObject);
        DemandOrderDO newPostOrderDO = new DemandOrderDO();
        Random random = new Random();
        String[] tags = {"萝莉","御姐","温柔","豪爽","乐观"};
        newPostOrderDO.setId(1L);
        newPostOrderDO.setDemandTag(tags[random.nextInt(5)]);
        newPostOrderDO.setStatus("Y");
        requestOfDataAccess.setArgumentList(newPostOrderDO);
        String requestMsg = new Gson().toJson(requestOfDataAccess);
        httpClientSender.sendRequest(requestMsg);
    }

    private static void AcceptDealOrderThroughDemandOrder() throws Exception{
        RequestOfDataAccess requestOfDataAccess = new RequestOfDataAccess();
        requestOfDataAccess.setCommand(Command.AcceptDealOrderThroughDemandOrder);
        DealOrderDO newDealOrder = new DealOrderDO();
        newDealOrder.setDemandOid(1L);
        newDealOrder.setUserId(3L);
        newDealOrder.setPsyUid(5L);
        newDealOrder.setServiceTime(2.0f);
        requestOfDataAccess.setArgumentList(newDealOrder);
        String requestMsg = new Gson().toJson(requestOfDataAccess);
        httpClientSender.sendRequest(requestMsg);
    }

    private static void UpdateDealOrderByDataObject() throws Exception {
        RequestOfDataAccess requestOfDataAccess = new RequestOfDataAccess();
        requestOfDataAccess.setCommand(Command.UpdateDealOrderByDataObject);
        DealOrderDO dealOrderDO = new DealOrderDO();
        dealOrderDO.setId(1L);
        dealOrderDO.setUserEvlAll(5);
        dealOrderDO.setUserEvlAtt(5);
        dealOrderDO.setUserEvlHpy(4);
        dealOrderDO.setUserEvlPrf(5);
        requestOfDataAccess.setArgumentList(dealOrderDO);
        String requestMsg = new Gson().toJson(requestOfDataAccess);
        httpClientSender.sendRequest(requestMsg);
    }

    private static void GetDealOrdersByUserId() throws Exception {
        RequestOfDataAccess requestOfDataAccess = new RequestOfDataAccess();
        requestOfDataAccess.setCommand(Command.GetDealOrdersByUserId);
        requestOfDataAccess.setArgumentList(3L);
        String requestMsg = new Gson().toJson(requestOfDataAccess);
        httpClientSender.sendRequest(requestMsg);
    }

    private static void GetDealOrdersByPsyUId() throws Exception {
        RequestOfDataAccess requestOfDataAccess = new RequestOfDataAccess();
        requestOfDataAccess.setCommand(Command.GetDealOrdersByPsyUId);
        requestOfDataAccess.setArgumentList(5L);
        String requestMsg = new Gson().toJson(requestOfDataAccess);
        httpClientSender.sendRequest(requestMsg);
    }

    public static void main(String[] args) throws Exception {
        //httpClientSender.setServerHost("121.40.212.164");

        RegisterUserByDataObject();
        DisableUserByLoginIdAndPassword();
        UpdateUserByDataObject();
        GetUserByLoginIdAndPassword();
        VisitUserById();
        
        RegisterPsychologistByDataObject();
        DisablePsychologistByLoginIdAndPassword();
        UpdatePsychologistByDataObject();
        GetPsychologistByLoginIdAndPassword();
        VisitPsychologistById();
        GetRecommendedPsychologists();

        RegisterRelationshipByDataObject();
        UpdateRelationshipByDataObject();
        GetAllFriendsByUserId();
        GetAllFriendsByPsyUid();

        GetPhotosByUserId();

        BindTagByDataObject();
        UnBindTagByDataObject();
        GetTagsByPsyUId();
        
        PostDemandOrderByDataObject();
        CancelDemandOrderById();
        UpdateDemandOrderByDataObject();

        AcceptDealOrderThroughDemandOrder();
        UpdateDealOrderByDataObject();
        GetDealOrdersByUserId();
        GetDealOrdersByPsyUId();
    }
}