package com.banban.common.dataobjects;

import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2015/1/25.
 */
public class PsychologistDO extends UserDO{
    private String isInService = "N";
    private String qualification = "N";
    private String name;
    private String idCardNo;
    private String idCardPath;
    private String address;
    private Integer level;
    private Integer customerLimit;
    private Date gmtCreatePsy;
    private Date gmtModifiedPsy;
    private List<UserDO> userFriends;
    private List<TagDO> tags;

    public PsychologistDO() {}

    public PsychologistDO(PsychologistDO psychologistDO, ConstructorType type) {
        if(type == ConstructorType.CopyPublicProperties || type == ConstructorType.CopyPublicPropertiesForFriend ||
                type == ConstructorType.CopyALLProperties) {
            this.id = psychologistDO.getId();
            this.easemobId = psychologistDO.getEasemobId();
            this.loginId = psychologistDO.getLoginId();
            this.status = psychologistDO.getStatus();
            this.nickName = psychologistDO.getNickName();
            this.gender = psychologistDO.getGender();
            this.portraitPath = psychologistDO.getPortraitPath();
            this.signature = psychologistDO.getSignature();
            this.isInService = psychologistDO.getIsInService();
            this.qualification =psychologistDO.getQualification();
            this.level =psychologistDO.getLevel();
        }
        if(type == ConstructorType.CopyPublicPropertiesForFriend || type == ConstructorType.CopyALLProperties) {
            this.cellphone = psychologistDO.getCellphone();
            this.birthday = psychologistDO.getBirthday();
            this.city = psychologistDO.getCity();
            this.gmtCreate = psychologistDO.getGmtCreate();
            this.gmtCreatePsy = psychologistDO.getGmtCreatePsy();
        }
        if(type == ConstructorType.CopyALLProperties) {
            this.psyId = psychologistDO.getPsyId();
            this.password = psychologistDO.getPassword();
            this.email = psychologistDO.getEmail();
            this.coin = psychologistDO.getCoin();
            this.freeCoin = psychologistDO.getFreeCoin();
            this.cashAccountId = psychologistDO.getCashAccountId();
            this.payPassword = psychologistDO.getPayPassword();
            this.gmtModified = psychologistDO.getGmtModified();
            this.name = psychologistDO.getName();
            this.idCardNo = psychologistDO.getIdCardNo();
            this.idCardPath = psychologistDO.getIdCardPath();
            this.address = psychologistDO.getAddress();
            this.customerLimit = psychologistDO.getCustomerLimit();
            this.gmtModifiedPsy = psychologistDO.getGmtModifiedPsy();
        }
    }

    public String getIsInService() {
        return isInService;
    }

    public void setIsInService(String isInService) {
        this.isInService = isInService;
    }

    public String getQualification() {
        return qualification;
    }

    public void setQualification(String qualification) {
        this.qualification = qualification;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdCardNo() {
        return idCardNo;
    }

    public void setIdCardNo(String idCardNo) {
        this.idCardNo = idCardNo;
    }

    public String getIdCardPath() {
        return idCardPath;
    }

    public void setIdCardPath(String idCardPath) {
        this.idCardPath = idCardPath;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Integer getCustomerLimit() {
        return customerLimit;
    }

    public void setCustomerLimit(Integer customerLimit) {
        this.customerLimit = customerLimit;
    }

    public Date getGmtCreatePsy() {
        return gmtCreatePsy;
    }

    public void setGmtCreatePsy(Date gmtCreatePsy) {
        this.gmtCreatePsy = gmtCreatePsy;
    }

    public Date getGmtModifiedPsy() {
        return gmtModifiedPsy;
    }

    public void setGmtModifiedPsy(Date gmtModifiedPsy) {
        this.gmtModifiedPsy = gmtModifiedPsy;
    }

    public List<UserDO> getUserFriends() {
        return userFriends;
    }

    public void setUserFriends(List<UserDO> userFriends) {
        this.userFriends = userFriends;
    }

    public List<TagDO> getTags() {
        return tags;
    }

    public void setTags(List<TagDO> tags) {
        this.tags = tags;
    }
}