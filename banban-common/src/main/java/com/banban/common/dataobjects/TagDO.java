package com.banban.common.dataobjects;

import java.util.Date;

/**
 * Created by Administrator on 2015/1/25.
 */
public class TagDO {
    public final static String[] Tags = {"萝莉","御姐","温柔","豪爽","乐观"};
    private Long id;
    private Long psyUid;
    private String tag;
    private Date gmtCreate;

    public Long getPsyUid() {
        return psyUid;
    }

    public void setPsyUid(Long psyUid) {
        this.psyUid = psyUid;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }
}