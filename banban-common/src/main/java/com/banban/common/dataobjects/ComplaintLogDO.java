package com.banban.common.dataobjects;

import java.util.Date;

/**
 * Created by Administrator on 2015/1/25.
 */
public class ComplaintLogDO {
    private Long id;
    private Long userId;
    private Long psyUid;
    private Integer type = 1;
    private String content;
    private Date gmtCreate;
    private Date gmtModified;
    private Date judge_gmt_start;
    private Date judge_gmt_end;
    private String judge_result_mark;
    private String judge_comment;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getPsyUid() {
        return psyUid;
    }

    public void setPsyUid(Long psyUid) {
        this.psyUid = psyUid;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    public Date getJudge_gmt_start() {
        return judge_gmt_start;
    }

    public void setJudge_gmt_start(Date judge_gmt_start) {
        this.judge_gmt_start = judge_gmt_start;
    }

    public Date getJudge_gmt_end() {
        return judge_gmt_end;
    }

    public void setJudge_gmt_end(Date judge_gmt_end) {
        this.judge_gmt_end = judge_gmt_end;
    }

    public String getJudge_result_mark() {
        return judge_result_mark;
    }

    public void setJudge_result_mark(String judge_result_mark) {
        this.judge_result_mark = judge_result_mark;
    }

    public String getJudge_comment() {
        return judge_comment;
    }

    public void setJudge_comment(String judge_comment) {
        this.judge_comment = judge_comment;
    }
}