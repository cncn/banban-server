package com.banban.common.dataobjects;

import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2015/1/25.
 */
public class UserDO {
    protected Long id;
    protected Long psyId;
    protected String easemobId;
    protected String loginId;
    protected String password;
    protected String cellphone;
    protected String status = "Y";
    protected String nickName;
    protected String gender;
    protected Date birthday;
    protected String email;
    protected String city;
    protected String portraitPath;
    protected String signature;
    protected Float coin = 0f;
    protected Float freeCoin = 0f;
    protected Long cashAccountId;
    protected String payPassword;
    protected Date gmtCreate;
    protected Date gmtModified;
    private List<PsychologistDO> psyFriends;

    public static enum ConstructorType {
        CopyPublicProperties, CopyPublicPropertiesForFriend, CopyALLProperties
    }

    public UserDO() {}

    public UserDO(UserDO userDO, ConstructorType type) {
        if(type == ConstructorType.CopyPublicProperties || type == ConstructorType.CopyPublicPropertiesForFriend ||
                type == ConstructorType.CopyALLProperties) {
            this.id = userDO.getId();
            this.easemobId = userDO.getEasemobId();
            this.loginId = userDO.getLoginId();
            this.status = userDO.getStatus();
            this.nickName = userDO.getNickName();
            this.gender = userDO.getGender();
            this.portraitPath = userDO.getPortraitPath();
            this.signature = userDO.getSignature();
        }
        if(type == ConstructorType.CopyPublicPropertiesForFriend || type == ConstructorType.CopyALLProperties) {
            this.cellphone = userDO.getCellphone();
            this.birthday = userDO.getBirthday();
            this.city = userDO.getCity();
            this.gmtCreate = userDO.getGmtCreate();
        }
        if(type == ConstructorType.CopyALLProperties) {
            this.psyId = userDO.getPsyId();
            this.password = userDO.getPassword();
            this.email = userDO.getEmail();
            this.coin = userDO.getCoin();
            this.freeCoin = userDO.getFreeCoin();
            this.cashAccountId = userDO.getCashAccountId();
            this.payPassword = userDO.getPayPassword();
            this.gmtModified = userDO.getGmtModified();
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPsyId() {
        return psyId;
    }

    public void setPsyId(Long psyId) {
        this.psyId = psyId;
    }

    public String getEasemobId() {
        return easemobId;
    }

    public void setEasemobId(String easemobId) {
        this.easemobId = easemobId;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCellphone() {
        return cellphone;
    }

    public void setCellphone(String cellphone) {
        this.cellphone = cellphone;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPortraitPath() {
        return portraitPath;
    }

    public void setPortraitPath(String portraitPath) {
        this.portraitPath = portraitPath;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public Float getCoin() {
        return coin;
    }

    public void setCoin(Float coin) {
        this.coin = coin;
    }

    public Float getFreeCoin() {
        return freeCoin;
    }

    public void setFreeCoin(Float freeCoin) {
        this.freeCoin = freeCoin;
    }

    public Long getCashAccountId() {
        return cashAccountId;
    }

    public void setCashAccountId(Long cashAccountId) {
        this.cashAccountId = cashAccountId;
    }

    public String getPayPassword() {
        return payPassword;
    }

    public void setPayPassword(String payPassword) {
        this.payPassword = payPassword;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    public List<PsychologistDO> getPsyFriends() {
        return psyFriends;
    }

    public void setPsyFriends(List<PsychologistDO> psyFriends) {
        this.psyFriends = psyFriends;
    }
}