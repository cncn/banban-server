package com.banban.common.dataobjects;

import java.util.Date;

/**
 * Created by Administrator on 2015/1/25.
 */
public class DealOrderDO {
    private Long id;
    private Long demandOid;
    private Long userId;
    private Long psyUid;
    private Float serviceTime;
    private Long discountId;
    private Date gmtCreate;
    private Date gmtModified;
    private Integer userEvlAll;
    private Integer userEvlAtt;
    private Integer userEvlHpy;
    private Integer userEvlPrf;
    private String userEvlCmt;
    private Date gmtUserEvl;
    private Integer psyEvlAll;
    private Integer psyEvlLtr;
    private Integer psyEvlFit;
    private Date gmtPsyEvl;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getDemandOid() {
        return demandOid;
    }

    public void setDemandOid(Long demandOid) {
        this.demandOid = demandOid;
    }

    public Long getPsyUid() {
        return psyUid;
    }

    public void setPsyUid(Long psyUid) {
        this.psyUid = psyUid;
    }

    public Float getServiceTime() {
        return serviceTime;
    }

    public void setServiceTime(Float serviceTime) {
        this.serviceTime = serviceTime;
    }

    public Long getDiscountId() {
        return discountId;
    }

    public void setDiscountId(Long discountId) {
        this.discountId = discountId;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    public Integer getUserEvlAll() {
        return userEvlAll;
    }

    public void setUserEvlAll(Integer userEvlAll) {
        this.userEvlAll = userEvlAll;
    }

    public Integer getUserEvlAtt() {
        return userEvlAtt;
    }

    public void setUserEvlAtt(Integer userEvlAtt) {
        this.userEvlAtt = userEvlAtt;
    }

    public Integer getUserEvlHpy() {
        return userEvlHpy;
    }

    public void setUserEvlHpy(Integer userEvlHpy) {
        this.userEvlHpy = userEvlHpy;
    }

    public Integer getUserEvlPrf() {
        return userEvlPrf;
    }

    public void setUserEvlPrf(Integer userEvlPrf) {
        this.userEvlPrf = userEvlPrf;
    }

    public String getUserEvlCmt() {
        return userEvlCmt;
    }

    public void setUserEvlCmt(String userEvlCmt) {
        this.userEvlCmt = userEvlCmt;
    }

    public Date getGmtUserEvl() {
        return gmtUserEvl;
    }

    public void setGmtUserEvl(Date gmtUserEvl) {
        this.gmtUserEvl = gmtUserEvl;
    }

    public Integer getPsyEvlAll() {
        return psyEvlAll;
    }

    public void setPsyEvlAll(Integer psyEvlAll) {
        this.psyEvlAll = psyEvlAll;
    }

    public Integer getPsyEvlLtr() {
        return psyEvlLtr;
    }

    public void setPsyEvlLtr(Integer psyEvlLtr) {
        this.psyEvlLtr = psyEvlLtr;
    }

    public Integer getPsyEvlFit() {
        return psyEvlFit;
    }

    public void setPsyEvlFit(Integer psyEvlFit) {
        this.psyEvlFit = psyEvlFit;
    }

    public Date getGmtPsyEvl() {
        return gmtPsyEvl;
    }

    public void setGmtPsyEvl(Date gmtPsyEvl) {
        this.gmtPsyEvl = gmtPsyEvl;
    }
}