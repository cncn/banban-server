package com.banban.common.dataobjects;

import java.util.Date;

/**
 * Created by Administrator on 2015/1/25.
 */
public class DemandOrderDO {
    private Long id;
    private Long userId;
    private String demandGender;
    private String demandTag;
    private String demandRemark;
    private Float demandTime;
    private Long discountId;
    private String status = "N";
    private Date gmtCreate;
    private Date gmtModified;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getDemandGender() {
        return demandGender;
    }

    public void setDemandGender(String demandGender) {
        this.demandGender = demandGender;
    }

    public String getDemandTag() {
        return demandTag;
    }

    public void setDemandTag(String demandTag) {
        this.demandTag = demandTag;
    }

    public String getDemandRemark() {
        return demandRemark;
    }

    public void setDemandRemark(String demandRemark) {
        this.demandRemark = demandRemark;
    }

    public Float getDemandTime() {
        return demandTime;
    }

    public void setDemandTime(Float demandTime) {
        this.demandTime = demandTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getDiscountId() {
        return discountId;
    }

    public void setDiscountId(Long discountId) {
        this.discountId = discountId;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }
}