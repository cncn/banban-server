package com.banban.common.request.arguments;

import java.io.Serializable;

/**
 * Created by Administrator on 2015/1/2.
 */
public class ArgsForGetUserById implements Serializable{

    Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
