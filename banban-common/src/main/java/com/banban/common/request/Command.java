package com.banban.common.request;

/**
 * Created by Administrator on 2015/1/5.
 */
public class Command {
    public static final int RegisterUserByDataObject = 101;
    public static final int DisableUserByLoginIdAndPassword = 102;
    public static final int UpdateUserByDataObject = 103;
    public static final int GetUserByLoginIdAndPassword = 104;
    public static final int VisitUserById = 105;

    public static final int RegisterPsychologistByDataObject = 111;
    public static final int DisablePsychologistByLoginIdAndPassword = 112;
    public static final int UpdatePsychologistByDataObject = 113;
    public static final int GetPsychologistByLoginIdAndPassword = 114;
    public static final int VisitPsychologistById = 115;
    public static final int GetRecommendedPsychologists = 116;

    public static final int RegisterRelationshipByDataObject = 121;
    public static final int UpdateRelationshipByDataObject = 122;
    public static final int GetAllFriendsByUserId = 123;
    public static final int GetAllFriendsByPsyUid = 124;

    public static final int UploadPhotoByDataObject = 131;
    public static final int DeletePhotoByDataObject = 132;
    public static final int GetPhotosByUserId = 133;

    public static final int BindTagByDataObject = 141;
    public static final int UnBindTagByDataObject = 142;
    public static final int GetTagsByPsyUId = 143;

    public static final int PostDemandOrderByDataObject = 151;
    public static final int CancelDemandOrderById = 152;
    public static final int UpdateDemandOrderByDataObject = 153;

    public static final int AcceptDealOrderThroughDemandOrder = 161;
    public static final int AcceptDealOrderThroughRequest = 162;
    public static final int UpdateDealOrderByDataObject = 163;
    public static final int GetDealOrdersByUserId = 164;
    public static final int GetDealOrdersByPsyUId = 165;
}