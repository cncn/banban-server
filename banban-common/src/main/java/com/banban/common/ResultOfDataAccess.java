package com.banban.common;

import java.io.Serializable;

/**
 * Created by Administrator on 2014/12/5.
 */
public class ResultOfDataAccess implements Serializable{

    boolean isSuccess = false;
    int commandInRequest = -1;
    Object result = null;

    public int getCommandInRequest() {
        return commandInRequest;
    }

    public void setCommandInRequest(int commandInRequest) {
        this.commandInRequest = commandInRequest;
    }

    public boolean isSuccess() {
        return isSuccess;
    }

    public void setSuccess(boolean isSuccess) {
        this.isSuccess = isSuccess;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }
}
