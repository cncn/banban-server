package com.banban.common;

import java.io.Serializable;

/**
 * Created by Administrator on 2015/1/2.
 */
public class RequestOfDataAccess implements Serializable{

    int command = -1;
    Object argumentList = null;

    public int getCommand() {
        return command;
    }

    public void setCommand(int command) {
        this.command = command;
    }

    public Object getArgumentList() {
        return argumentList;
    }

    public void setArgumentList(Object argumentList) {
        this.argumentList = argumentList;
    }
}
