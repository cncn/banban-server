package com.banban.server.net.http;

import com.banban.common.RequestOfDataAccess;
import com.banban.common.ResultOfDataAccess;
import com.banban.common.utils.ByteBufToBytes;
import com.banban.server.common.HandlerCenter;
import com.google.gson.Gson;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.codec.http.*;
import io.netty.handler.codec.http.HttpHeaders.Values;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;

import static io.netty.handler.codec.http.HttpHeaders.Names.*;
import static io.netty.handler.codec.http.HttpResponseStatus.OK;
import static io.netty.handler.codec.http.HttpVersion.HTTP_1_1;

/**
 * Created by Administrator on 2014/11/29.
 */
@ChannelHandler.Sharable
public class HttpServerInboundHandler extends ChannelInboundHandlerAdapter {
    private static Logger	logger	= LoggerFactory.getLogger(HttpServerInboundHandler.class);
    private ByteBufToBytes reader;

    public void setHandlerCenter(HandlerCenter handlerCenter) {
        this.handlerCenter = handlerCenter;
    }

    private HandlerCenter handlerCenter;

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        if (msg instanceof HttpRequest) {
            HttpRequest request = (HttpRequest) msg;
            System.out.println("\nmessageType:" + request.headers().get("messageType"));
            System.out.println("businessType:" + request.headers().get("businessType"));
            if (HttpHeaders.isContentLengthSet(request)) {
                reader = new ByteBufToBytes((int) HttpHeaders.getContentLength(request));
            }
        }

        if (msg instanceof HttpContent) {
            HttpContent httpContent = (HttpContent) msg;
            ByteBuf content = httpContent.content();
            reader.reading(content);
            content.release();

            if (reader.isEnd()) {
                String resultStr = new String(reader.readFull());
                SimpleDateFormat sDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                String curTime = sDateFormat.format(new java.util.Date());
                System.out.println("At " + curTime + ",Client said:" + resultStr);
                ResultOfDataAccess resultOfDataAccess = new ResultOfDataAccess();
                try {
                    RequestOfDataAccess requestOfDataAccess = new Gson().fromJson(resultStr, RequestOfDataAccess.class);
                    String jsonRequest = new Gson().toJson(requestOfDataAccess.getArgumentList());
                    try {
                        resultOfDataAccess = handlerCenter.excute(requestOfDataAccess.getCommand(), jsonRequest);
                    }
                    catch (Exception e) {
                        System.out.println("ERROR: " + e.toString());
                        resultOfDataAccess.setSuccess(false);
                        resultOfDataAccess.setCommandInRequest(requestOfDataAccess.getCommand());
                        resultOfDataAccess.setResult("ERROR: " + e.toString());
                    }
                }
                catch (Exception e) {
                    System.out.println("ERROR: " + e.toString());
                    resultOfDataAccess.setSuccess(false);
                    resultOfDataAccess.setCommandInRequest(0);
                    resultOfDataAccess.setResult("ERROR: " + e.toString());
                }
                String responseString = new Gson().toJson(resultOfDataAccess);
                FullHttpResponse response = new DefaultFullHttpResponse(HTTP_1_1, OK, Unpooled.wrappedBuffer(responseString.getBytes()));
                response.headers().set(CONTENT_TYPE, "text/plain");
                response.headers().set(CONTENT_LENGTH, response.content().readableBytes());
                response.headers().set(CONNECTION, Values.KEEP_ALIVE);
                ctx.write(response);
                ctx.flush();
            }
        }
    }

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
        logger.info("HttpServerInboundHandler.channelReadComplete");
        ctx.flush();
    }

}