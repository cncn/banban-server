package com.banban.server.common;

import com.banban.common.ResultOfDataAccess;

/**
 * Created by Administrator on 2014/12/6.
 */
public interface HandlerCenter {
    public ResultOfDataAccess excute(int command, String argument) throws Exception;
}
