package com.banban.server.dataaccess.object;

import com.banban.common.ResultOfDataAccess;
import com.banban.common.dataobjects.DemandOrderDO;
import com.banban.common.request.Command;
import org.junit.Test;

import java.sql.SQLException;
import java.util.Random;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by Administrator on 2015/2/8.
 */
public class DemandOrderDAOTest {

    @Test
    public void GetPostOrdersByUserId() throws SQLException {
        DemandOrderDO demandOrderDO = DemandOrderDAO.GetDemandOrderById(1L);
        assertEquals(3L, demandOrderDO.getUserId().longValue());
    }

    @Test
    public void PostDemandOrderByDataObject() throws Exception {
        DemandOrderDO newDemandOrderDO = new DemandOrderDO();
        Random random = new Random();
        int randomLength = random.nextInt(18) + 6;
        String demandRemark = "";
        for(int i = 0; i < randomLength; ++i) {
            char c = (char) (random.nextInt(26) + 97);
            demandRemark += c;
        }
        Long[] availableUserId = {3L, 4L};
        float[] availableTimeLength = {0.5f, 1.0f, 1.5f, 2.0f, 2.5f, 3.0f};
        String[] tags = {"萝莉","御姐","温柔","豪爽","乐观"};
        newDemandOrderDO.setUserId(availableUserId[random.nextInt(2)]);
        newDemandOrderDO.setDemandGender("MM");
        newDemandOrderDO.setDemandTag(tags[random.nextInt(5)]);
        newDemandOrderDO.setDemandRemark(demandRemark);
        newDemandOrderDO.setDemandTime(availableTimeLength[random.nextInt(6)]);
        newDemandOrderDO.setStatus("N");
        ResultOfDataAccess resultOfPostDemandOrder = DemandOrderDAO.PostDemandOrderByDataObject(newDemandOrderDO);
        assertEquals(Command.PostDemandOrderByDataObject, resultOfPostDemandOrder.getCommandInRequest());
        assertTrue(resultOfPostDemandOrder.isSuccess());
        Long newDemandOrderId = (Long) resultOfPostDemandOrder.getResult();
        assertTrue(newDemandOrderId > 0);
    }

    @Test
    public void CancelDemandOrderById() throws Exception {
        ResultOfDataAccess resultOfUpdateDemandOrder = DemandOrderDAO.CancelDemandOrderById(1L);
        assertEquals(Command.CancelDemandOrderById, resultOfUpdateDemandOrder.getCommandInRequest());
        assertTrue(resultOfUpdateDemandOrder.isSuccess());
        int affectedRowNum = (int) resultOfUpdateDemandOrder.getResult();
        assertTrue(affectedRowNum == 1);
    }

    @Test
    public void UpdateDemandOrderByDataObject() throws Exception {
        DemandOrderDO newDemandOrderDO = new DemandOrderDO();
        newDemandOrderDO.setId(1L);
        newDemandOrderDO.setStatus("Y");
        ResultOfDataAccess resultOfUpdateDemandOrder = DemandOrderDAO.UpdateDemandOrderByDataObject(newDemandOrderDO);
        assertEquals(Command.UpdateDemandOrderByDataObject, resultOfUpdateDemandOrder.getCommandInRequest());
        assertTrue(resultOfUpdateDemandOrder.isSuccess());
        int affectedRowNum = (int) resultOfUpdateDemandOrder.getResult();
        assertTrue(affectedRowNum == 1);
    }
}