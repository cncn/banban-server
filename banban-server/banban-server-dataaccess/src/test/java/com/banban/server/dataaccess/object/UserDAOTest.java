package com.banban.server.dataaccess.object;

import com.banban.common.ResultOfDataAccess;
import com.banban.common.dataobjects.UserDO;
import com.banban.common.request.Command;
import org.junit.Test;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Random;

import static org.junit.Assert.*;

/**
 * Created by Administrator on 2014/12/6.
 */
public class UserDAOTest {

    @Test
    public void IsUserExistByLoginId() throws SQLException {
        assertTrue(UserDAO.IsUserExistByLoginId("mxyydy"));
    }

    @Test
    public void RegisterUserByDataObject() throws Exception {
        UserDO availableNewUserDO = new UserDO();
        Random random = new Random();
        int randomLength = random.nextInt(4) + 6;
        String loginId = "";
        String nickName = "";
        for(int i = 0; i < randomLength; ++i) {
            loginId += (char) (random.nextInt(26) + 97);
            nickName += (char) (random.nextInt(26) + 97);
        }
        String subCellPhone = "";
        for(int i = 0; i < 4; ++i) subCellPhone += (char) (random.nextInt(10) + 48);
        char month  = (char) (random.nextInt(9) + 49);
        Date birthday = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse("1990-0" + month + "-01 00:00:00");
        availableNewUserDO.setLoginId(loginId);
        availableNewUserDO.setPassword("123456");
        availableNewUserDO.setCellphone("1381704" + subCellPhone);
        availableNewUserDO.setNickName(nickName);
        availableNewUserDO.setGender(random.nextBoolean() ? "GG" : "MM");
        availableNewUserDO.setBirthday(birthday);
        ResultOfDataAccess resultOfRegisterUser = UserDAO.RegisterUserByDataObject(availableNewUserDO);
        assertEquals(Command.RegisterUserByDataObject, resultOfRegisterUser.getCommandInRequest());
        assertTrue(resultOfRegisterUser.isSuccess());
        Long newId = (Long) resultOfRegisterUser.getResult();
        assertTrue(newId > 5L);
    }

    @Test
    public void DisableUserByLoginIdAndPassword() throws SQLException {
        HashMap<String, String> rightLoginIdAndPassword = new HashMap<>();
        rightLoginIdAndPassword.put("loginId", "mxyydy");
        rightLoginIdAndPassword.put("password", "123456");
        ResultOfDataAccess resultOfDisableUser1 = UserDAO.DisableUserByLoginIdAndPassword(rightLoginIdAndPassword);
        assertEquals(Command.DisableUserByLoginIdAndPassword, resultOfDisableUser1.getCommandInRequest());
        assertTrue(resultOfDisableUser1.isSuccess());
        int rowNum = (int) resultOfDisableUser1.getResult();
        assertTrue(rowNum == 1);

        HashMap<String, String> wrongLoginIdAndPassword = new HashMap<>();
        wrongLoginIdAndPassword.put("loginId", "mxyydy");
        wrongLoginIdAndPassword.put("password", "wrong");
        ResultOfDataAccess resultOfDisableUser2 = UserDAO.DisableUserByLoginIdAndPassword(wrongLoginIdAndPassword);
        assertEquals(Command.DisableUserByLoginIdAndPassword, resultOfDisableUser2.getCommandInRequest());
        assertFalse(resultOfDisableUser2.isSuccess());
        assertEquals("Warn: The loginId or password may be wrong!", resultOfDisableUser2.getResult());
    }

    @Test
    public void UpdateUserByDataObject() throws Exception {
        UserDO updatedUserDO = new UserDO();
        updatedUserDO.setId(2L);
        Random random = new Random();
        int randomLength = random.nextInt(4) + 6;
        String email = "";
        for(int i = 0; i < randomLength; ++i) email += (char) (random.nextInt(26) + 97);
        updatedUserDO.setEmail(email + "@gmail.com");
        updatedUserDO.setStatus("Y");
        ResultOfDataAccess resultOfUpdateUser = UserDAO.UpdateUserByDataObject(updatedUserDO);
        assertEquals(Command.UpdateUserByDataObject, resultOfUpdateUser.getCommandInRequest());
        assertTrue(resultOfUpdateUser.isSuccess());
        Integer affectedRowNum = (Integer) resultOfUpdateUser.getResult();
        assertTrue(affectedRowNum == 1);
    }

    @Test
    public void GetUserByLoginIdAndPassword() throws SQLException {
        HashMap<String, String> rightLoginIdAndPassword = new HashMap<>();
        rightLoginIdAndPassword.put("loginId", "mxyydy");
        rightLoginIdAndPassword.put("password", "123456");
        ResultOfDataAccess resultOfGetUser1 = UserDAO.GetUserByLoginIdAndPassword(rightLoginIdAndPassword);
        assertEquals(Command.GetUserByLoginIdAndPassword, resultOfGetUser1.getCommandInRequest());
        assertTrue(resultOfGetUser1.isSuccess());
        UserDO userDO = (UserDO) resultOfGetUser1.getResult();
        assertEquals("Journey", userDO.getNickName());
        assertEquals("GG", userDO.getGender());
        assertNotNull(userDO.getPsyFriends());

        HashMap<String, String> wrongLoginIdAndPassword = new HashMap<>();
        wrongLoginIdAndPassword.put("loginId", "mxyydy");
        wrongLoginIdAndPassword.put("password", "wrong");
        ResultOfDataAccess resultOfGetUser2 = UserDAO.GetUserByLoginIdAndPassword(wrongLoginIdAndPassword);
        assertEquals(Command.GetUserByLoginIdAndPassword, resultOfGetUser2.getCommandInRequest());
        assertFalse(resultOfGetUser2.isSuccess());
        assertEquals("Warn: The loginId or password may be wrong!", resultOfGetUser2.getResult());
    }

    @Test
    public void VisitUserById() throws SQLException {
        ResultOfDataAccess resultOfVisitUser = UserDAO.VisitUserById(2L);
        assertTrue(resultOfVisitUser.isSuccess());
        assertEquals(Command.VisitUserById, resultOfVisitUser.getCommandInRequest());
        UserDO userDO = (UserDO) resultOfVisitUser.getResult();
        assertEquals("mxyydy", userDO.getLoginId());
        assertEquals("Journey", userDO.getNickName());
        assertNull(userDO.getPassword());
        assertEquals("18205188113", userDO.getCellphone());
    }
}