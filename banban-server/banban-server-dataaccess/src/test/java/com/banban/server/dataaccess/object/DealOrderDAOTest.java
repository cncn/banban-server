package com.banban.server.dataaccess.object;

import com.banban.common.ResultOfDataAccess;
import com.banban.common.dataobjects.DealOrderDO;
import com.banban.common.request.Command;
import org.junit.Test;

import java.sql.SQLException;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by Administrator on 2015/2/13.
 */
public class DealOrderDAOTest {

    @Test
    public void AcceptDealOrderThroughDemandOrder() throws Exception{
        DealOrderDO newDealOrder = new DealOrderDO();
        newDealOrder.setDemandOid(1L);
        newDealOrder.setUserId(3L);
        newDealOrder.setPsyUid(5L);
        newDealOrder.setServiceTime(2.0f);
        ResultOfDataAccess resultOfAcceptDealOrder = DealOrderDAO.AcceptDealOrderThroughDemandOrder(newDealOrder);
        assertEquals(Command.AcceptDealOrderThroughDemandOrder, resultOfAcceptDealOrder.getCommandInRequest());
        if(resultOfAcceptDealOrder.isSuccess()) assertTrue(resultOfAcceptDealOrder.getResult() != null);
        else {
            String errorInfo = (String) resultOfAcceptDealOrder.getResult();
            assertEquals("The demand order has been processed!" ,errorInfo);
        }
    }

    @Test
    public void UpdateDealOrderByDataObject() throws SQLException {
        DealOrderDO dealOrderDO = new DealOrderDO();
        dealOrderDO.setId(1L);
        dealOrderDO.setUserEvlAll(5);
        dealOrderDO.setUserEvlAtt(5);
        dealOrderDO.setUserEvlHpy(4);
        dealOrderDO.setUserEvlPrf(5);
        ResultOfDataAccess resultOfUpdateDealOrder = DealOrderDAO.UpdateDealOrderByDataObject(dealOrderDO);
        assertEquals(Command.UpdateDealOrderByDataObject, resultOfUpdateDealOrder.getCommandInRequest());
        assertTrue(resultOfUpdateDealOrder.isSuccess());
        int affectedRowNum = (int) resultOfUpdateDealOrder.getResult();
        assertTrue(affectedRowNum == 1);
    }

    @Test
    public void GetDealOrdersByUserId() throws SQLException {
        ResultOfDataAccess resultOfGetDealOrders = DealOrderDAO.GetDealOrdersByUserId(3L);
        assertEquals(Command.GetDealOrdersByUserId, resultOfGetDealOrders.getCommandInRequest());
        assertTrue(resultOfGetDealOrders.isSuccess());
        List<DealOrderDO> dealOrderDOList = (List<DealOrderDO>) resultOfGetDealOrders.getResult();
        assertFalse(dealOrderDOList.isEmpty());
    }

    @Test
    public void GetDealOrdersByPsyUId() throws SQLException {
        ResultOfDataAccess resultOfGetDealOrders = DealOrderDAO.GetDealOrdersByPsyUId(5L);
        assertEquals(Command.GetDealOrdersByPsyUId, resultOfGetDealOrders.getCommandInRequest());
        assertTrue(resultOfGetDealOrders.isSuccess());
        List<DealOrderDO> dealOrderDOList = (List<DealOrderDO>) resultOfGetDealOrders.getResult();
        assertFalse(dealOrderDOList.isEmpty());
    }
}