package com.banban.server.dataaccess.object;

import com.banban.common.ResultOfDataAccess;
import com.banban.common.dataobjects.PsychologistDO;
import com.banban.common.dataobjects.RelationshipDO;
import com.banban.common.dataobjects.UserDO;
import com.banban.common.request.Command;
import org.junit.Test;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import static org.junit.Assert.*;

/**
 * Created by Administrator on 2014/12/6.
 */
public class RelationshipDAOTest {

    @Test
    public void GetRelationshipByUserIdAndPsyUid() throws SQLException {
        HashMap<String, Long> existentUserIdAndPsyUid = new HashMap<>();
        existentUserIdAndPsyUid.put("userId", 2L);
        existentUserIdAndPsyUid.put("psyUid", 1L);
        RelationshipDO relationshipDO1 = RelationshipDAO.GetRelationshipByUserIdAndPsyUid(existentUserIdAndPsyUid);
        assertNotNull(relationshipDO1);
        assertEquals(1, relationshipDO1.getType().intValue());

        HashMap<String, Long> nonexistentUserIdAndPsyUid = new HashMap<>();
        nonexistentUserIdAndPsyUid.put("userId", 0L);
        nonexistentUserIdAndPsyUid.put("psyUid", 1L);
        RelationshipDO relationshipDO2 = RelationshipDAO.GetRelationshipByUserIdAndPsyUid(nonexistentUserIdAndPsyUid);
        assertNull(relationshipDO2);
    }

    @Test
    public void GetRelationshipByUserId() throws SQLException {
        Long existentUserId = 2L;
        List<RelationshipDO> relationshipDOList1 = RelationshipDAO.GetRelationshipByUserId(existentUserId);
        assertTrue(relationshipDOList1.size() >= 1);

        Long nonexistentUserId = 0L;
        List<RelationshipDO> relationshipDOList2 = RelationshipDAO.GetRelationshipByUserId(nonexistentUserId);
        assertTrue(relationshipDOList2.size() == 0);
    }

    @Test
    public void GetRelationshipByPsyUid() throws SQLException {
        Long existentPsyUid = 1L;
        List<RelationshipDO> relationshipDOList1 = RelationshipDAO.GetRelationshipByPsyUid(existentPsyUid);
        assertTrue(relationshipDOList1.size() >= 1);

        Long nonexistentPsyUid = 0L;
        List<RelationshipDO> relationshipDOList2 = RelationshipDAO.GetRelationshipByPsyUid(nonexistentPsyUid);
        assertTrue(relationshipDOList2.size() == 0);
    }

    @Test
    public void RegisterRelationshipByDataObject() throws Exception {
        RelationshipDO newRelationshipDO = new RelationshipDO();
        newRelationshipDO.setUserId(1L);
        newRelationshipDO.setPsyUid(2L);
        newRelationshipDO.setType(1);
        ResultOfDataAccess resultOfRegisterRelationship = RelationshipDAO.RegisterRelationshipByDataObject(newRelationshipDO);
        assertEquals(Command.RegisterRelationshipByDataObject, resultOfRegisterRelationship.getCommandInRequest());
        if(resultOfRegisterRelationship.isSuccess()) {
            Long newId = (Long) resultOfRegisterRelationship.getResult();
            assertTrue(newId == 1L);
        } else {
            assertEquals("The relationship has existed!", resultOfRegisterRelationship.getResult().toString());
        }
    }

    @Test
    public void UpdateRelationshipByDataObject() throws Exception {
        RelationshipDO relationshipDO = new RelationshipDO();
        relationshipDO.setUserId(4L);
        relationshipDO.setPsyUid(5L);
        relationshipDO.setType(new Random().nextBoolean() ? 1 : 2);
        ResultOfDataAccess resultOfUpdateRelationship = RelationshipDAO.UpdateRelationshipByDataObject(relationshipDO);
        assertEquals(Command.UpdateRelationshipByDataObject, resultOfUpdateRelationship.getCommandInRequest());
        assertTrue(resultOfUpdateRelationship.isSuccess());
        int affectedRowNum = (int) resultOfUpdateRelationship.getResult();
        assertTrue(affectedRowNum == 1L);
    }

    @Test
    public void GetAllFriendsByUserId() throws Exception {
        ResultOfDataAccess resultOfGetAllFriends = RelationshipDAO.GetAllFriendsByUserId(2L);
        assertEquals(Command.GetAllFriendsByUserId, resultOfGetAllFriends.getCommandInRequest());
        assertTrue(resultOfGetAllFriends.isSuccess());
        List<PsychologistDO> psychologistDOList = (List<PsychologistDO>) resultOfGetAllFriends.getResult();
        assertTrue(psychologistDOList.size() >= 1);
    }

    @Test
    public void GetAllFriendsByPsyUid() throws Exception {
        ResultOfDataAccess resultOfGetAllFriends = RelationshipDAO.GetAllFriendsByPsyUid(1L);
        assertEquals(Command.GetAllFriendsByPsyUid, resultOfGetAllFriends.getCommandInRequest());
        assertTrue(resultOfGetAllFriends.isSuccess());
        List<UserDO> userDOList = (List<UserDO>) resultOfGetAllFriends.getResult();
        assertTrue(userDOList.size() >= 1);
    }
}