package com.banban.server.dataaccess.object;

import com.banban.common.ResultOfDataAccess;
import com.banban.common.dataobjects.PsychologistDO;
import com.banban.common.request.Command;
import org.junit.Test;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import static org.junit.Assert.*;

/**
 * Created by Administrator on 2015/2/10.
 */
public class PsychologistDAOTest {

    @Test
    public void RegisterPsychologistByDataObject() throws Exception {
        PsychologistDO availableNewPsychologistDO = new PsychologistDO();
        Random random = new Random();
        int randomLength = random.nextInt(4) + 6;
        String loginId = "";
        String nickName = "";
        for(int i = 0; i < randomLength; ++i) {
            loginId += (char) (random.nextInt(26) + 97);
            nickName += (char) (random.nextInt(26) + 97);
        }
        String subCellPhone = "";
        for(int i = 0; i < 4; ++i) subCellPhone += (char) (random.nextInt(10) + 48);
        char month  = (char) (random.nextInt(9) + 49);
        Date birthday = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse("1990-0" + month + "-01 00:00:00");
        availableNewPsychologistDO.setLoginId(loginId);
        availableNewPsychologistDO.setPassword("123456");
        availableNewPsychologistDO.setCellphone("1381704" + subCellPhone);
        availableNewPsychologistDO.setNickName(nickName);
        availableNewPsychologistDO.setGender("MM");
        availableNewPsychologistDO.setBirthday(birthday);

        String is_in_service;
        String qualification = random.nextBoolean() ? "Y":"N";
        if(qualification.equals("N")) is_in_service = "N";
        else is_in_service = new Random().nextBoolean() ? "Y":"N";
        availableNewPsychologistDO.setIsInService(is_in_service);
        availableNewPsychologistDO.setQualification(qualification);
        String name = "";
        String[] words = {"向", "雪", "贾", "萱", "李", "晓", "白", "梦", "琳", "葱"};
        int lengthOfName = random.nextInt(3) + 2;
        for(int i = 0; i < lengthOfName; ++i) name += words[random.nextInt(10)];
        availableNewPsychologistDO.setName(name);
        String idCardNo = "";
        for(int i = 0; i < 18; ++i) idCardNo += (char) (random.nextInt(10) + 48);
        availableNewPsychologistDO.setIdCardNo(idCardNo);
        if(qualification.equals("Y")) {
            String idCardPath = "./id_card_";
            for(int i = 0; i < 5; ++i) idCardPath += (char) (random.nextInt(10) + 48);
            idCardPath += ".jpg";
            availableNewPsychologistDO.setIdCardPath(idCardPath);
            availableNewPsychologistDO.setLevel(random.nextInt(5) + 1);
            availableNewPsychologistDO.setCustomerLimit(random.nextInt(5) + 5);
        }
        ResultOfDataAccess resultOfRegisterPsychologist = PsychologistDAO.RegisterPsychologistByDataObject(availableNewPsychologistDO);
        assertTrue(resultOfRegisterPsychologist.isSuccess());
        assertEquals(Command.RegisterPsychologistByDataObject, resultOfRegisterPsychologist.getCommandInRequest());
        Long newId = (Long) resultOfRegisterPsychologist.getResult();
        assertTrue(newId > 5L);
    }

    @Test
    public void DisablePsychologistByLoginIdAndPassword() throws SQLException {
        HashMap<String, String> rightLoginIdAndPassword = new HashMap<>();
        rightLoginIdAndPassword.put("loginId", "leonili");
        rightLoginIdAndPassword.put("password", "123456");
        ResultOfDataAccess resultOfDisablePsychologist1 = PsychologistDAO.DisablePsychologistByLoginIdAndPassword(rightLoginIdAndPassword);
        assertEquals(Command.DisablePsychologistByLoginIdAndPassword, resultOfDisablePsychologist1.getCommandInRequest());
        assertTrue(resultOfDisablePsychologist1.isSuccess());
        int rowNum = (int) resultOfDisablePsychologist1.getResult();
        assertTrue(rowNum == 1);

        HashMap<String, String> wrongLoginIdAndPassword = new HashMap<>();
        wrongLoginIdAndPassword.put("loginId", "mxyydy");
        wrongLoginIdAndPassword.put("password", "123456");
        ResultOfDataAccess resultOfDisablePsychologist2 = PsychologistDAO.DisablePsychologistByLoginIdAndPassword(wrongLoginIdAndPassword);
        assertEquals(Command.DisablePsychologistByLoginIdAndPassword, resultOfDisablePsychologist2.getCommandInRequest());
        assertFalse(resultOfDisablePsychologist2.isSuccess());
        assertEquals("Warn: The user isn't psychologist!", resultOfDisablePsychologist2.getResult());
    }

    @Test
    public void UpdatePsychologistByDataObject() throws Exception {
        PsychologistDO updatedPsychologistDO = new PsychologistDO();
        updatedPsychologistDO.setPsyId(1L);
        updatedPsychologistDO.setIsInService("Y");
        updatedPsychologistDO.setQualification("Y");
        ResultOfDataAccess resultOfUpdatePsychologist = PsychologistDAO.UpdatePsychologistByDataObject(updatedPsychologistDO);
        assertEquals(Command.UpdatePsychologistByDataObject, resultOfUpdatePsychologist.getCommandInRequest());
        assertTrue(resultOfUpdatePsychologist.isSuccess());
        Integer affectedRowNum = (Integer) resultOfUpdatePsychologist.getResult();
        assertTrue(affectedRowNum == 1);
    }

    @Test
    public void GetPsychologistByLoginIdAndPassword() throws SQLException {
        HashMap<String, String> rightLoginIdAndPassword = new HashMap<>();
        rightLoginIdAndPassword.put("loginId", "leonili");
        rightLoginIdAndPassword.put("password", "123456");
        ResultOfDataAccess resultOfGetPsychologist1 = PsychologistDAO.GetPsychologistByLoginIdAndPassword(rightLoginIdAndPassword);
        assertEquals(Command.GetPsychologistByLoginIdAndPassword, resultOfGetPsychologist1.getCommandInRequest());
        assertTrue(resultOfGetPsychologist1.isSuccess());
        PsychologistDO psychologistDO = (PsychologistDO) resultOfGetPsychologist1.getResult();
        assertEquals("Fotia", psychologistDO.getNickName());
        assertEquals("MM", psychologistDO.getGender());
        assertEquals(5, psychologistDO.getLevel().intValue());
        assertNotNull(psychologistDO.getUserFriends());
        assertNotNull(psychologistDO.getTags());

        HashMap<String, String> wrongLoginIdAndPassword = new HashMap<>();
        wrongLoginIdAndPassword.put("loginId", "mxyydy");
        wrongLoginIdAndPassword.put("password", "123456");
        ResultOfDataAccess resultOfGetPsychologist2 = PsychologistDAO.GetPsychologistByLoginIdAndPassword(wrongLoginIdAndPassword);
        assertEquals(Command.GetPsychologistByLoginIdAndPassword, resultOfGetPsychologist2.getCommandInRequest());
        assertFalse(resultOfGetPsychologist2.isSuccess());
        assertEquals("Warn: The loginId or password may be wrong!", resultOfGetPsychologist2.getResult());
    }

    @Test
    public void VisitPsychologistById() throws SQLException {
        ResultOfDataAccess resultOfVisitPsychologist = PsychologistDAO.VisitPsychologistById(1L);
        assertTrue(resultOfVisitPsychologist.isSuccess());
        assertEquals(Command.VisitPsychologistById, resultOfVisitPsychologist.getCommandInRequest());
        PsychologistDO psychologistDO = (PsychologistDO) resultOfVisitPsychologist.getResult();
        assertEquals("leonili", psychologistDO.getLoginId());
        assertEquals("Fotia", psychologistDO.getNickName());
        assertNull(psychologistDO.getPassword());
        assertEquals("15298386850", psychologistDO.getCellphone());
        assertNotNull(psychologistDO.getGmtCreatePsy());
    }

    @Test
    public void GetRecommendedPsychologists() throws SQLException {
        ResultOfDataAccess resultOfGetPsy = PsychologistDAO.GetRecommendedPsychologists();
        assertTrue(resultOfGetPsy.isSuccess());
        assertEquals(Command.GetRecommendedPsychologists, resultOfGetPsy.getCommandInRequest());
        List<PsychologistDO> psychologistDOList = (List<PsychologistDO>) resultOfGetPsy.getResult();
        assertTrue(psychologistDOList.size() >= 0);
    }
}