package com.banban.server.dataaccess.object;

import com.banban.common.ResultOfDataAccess;
import com.banban.common.dataobjects.PsychologistDO;
import com.banban.common.dataobjects.TagDO;
import com.banban.common.request.Command;
import org.junit.Test;

import java.sql.SQLException;
import java.util.List;
import java.util.Random;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * Created by Administrator on 2015/2/13.
 */
public class TagDAOTest {

    @Test
    public void BindTagByDataObject() throws SQLException {
        TagDO newTagDO = new TagDO();
        Random random = new Random();
        ResultOfDataAccess resultOfGetPsy = PsychologistDAO.GetRecommendedPsychologists();
        List<PsychologistDO> psychologistDOList = (List<PsychologistDO>) resultOfGetPsy.getResult();
        newTagDO.setPsyUid(psychologistDOList.get(random.nextInt(psychologistDOList.size())).getId());
        newTagDO.setTag(TagDO.Tags[random.nextInt(TagDO.Tags.length)]);
        ResultOfDataAccess resultOfDataAccess = TagDAO.BindTagByDataObject(newTagDO);
        assertEquals(Command.BindTagByDataObject, resultOfDataAccess.getCommandInRequest());
    }

    @Test
    public void UnBindTagByDataObject() throws SQLException {
        TagDO newTagDO = new TagDO();
        Random random = new Random();
        ResultOfDataAccess resultOfGetPsy = PsychologistDAO.GetRecommendedPsychologists();
        List<PsychologistDO> psychologistDOList = (List<PsychologistDO>) resultOfGetPsy.getResult();
        newTagDO.setPsyUid(psychologistDOList.get(random.nextInt(psychologistDOList.size())).getId());
        newTagDO.setTag(TagDO.Tags[random.nextInt(TagDO.Tags.length)]);
        ResultOfDataAccess resultOfUnBindTag = TagDAO.UnBindTagByDataObject(newTagDO);
        assertEquals(Command.UnBindTagByDataObject, resultOfUnBindTag.getCommandInRequest());
    }

    @Test
    public void GetTagsByPsyUId() throws SQLException {
        Long psyUIid = 1L;
        ResultOfDataAccess resultOfUnBindTag = TagDAO.GetTagsByPsyUId(psyUIid);
        assertEquals(Command.GetTagsByPsyUId, resultOfUnBindTag.getCommandInRequest());
        assertTrue(resultOfUnBindTag.isSuccess());
        List<TagDO> tagDOList = (List<TagDO>) resultOfUnBindTag.getResult();
        assertTrue(tagDOList.size() >= 0);
    }
}