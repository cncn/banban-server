package com.banban.server.dataaccess.object;

import com.banban.common.ResultOfDataAccess;
import com.banban.common.dataobjects.PhotoDO;
import com.banban.common.request.Command;
import com.ibatis.common.resources.Resources;
import com.ibatis.sqlmap.client.SqlMapClient;
import com.ibatis.sqlmap.client.SqlMapClientBuilder;

import java.io.IOException;
import java.io.Reader;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Administrator on 2015/2/6.
 */
public class PhotoDAO {

    private static SqlMapClient sqlMapClient = null;

    static {
        Reader reader = null;
        try {
            reader = Resources.getResourceAsReader("ibatis/sql-map-config.xml");
        }
        catch (IOException e) {
            System.out.println(e.toString());
        }
        sqlMapClient = SqlMapClientBuilder.buildSqlMapClient(reader);
    }

    public static ResultOfDataAccess GetPhotosByUserId(Long userId) throws SQLException {
        ResultOfDataAccess resultOfDataAccess = new ResultOfDataAccess();
        resultOfDataAccess.setCommandInRequest(Command.GetPhotosByUserId);
        List<PhotoDO> photoDOList = sqlMapClient.queryForList("GetPhotosByUserId", userId);
        resultOfDataAccess.setSuccess(true);
        resultOfDataAccess.setResult(photoDOList);
        return resultOfDataAccess;
    }
}