package com.banban.server.dataaccess.object;

import com.banban.common.ResultOfDataAccess;
import com.banban.common.dataobjects.TagDO;
import com.banban.common.request.Command;
import com.ibatis.common.resources.Resources;
import com.ibatis.sqlmap.client.SqlMapClient;
import com.ibatis.sqlmap.client.SqlMapClientBuilder;

import java.io.IOException;
import java.io.Reader;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Administrator on 2015/2/13.
 */
public class TagDAO {

    private static SqlMapClient sqlMapClient = null;

    static {
        Reader reader = null;
        try {
            reader = Resources.getResourceAsReader("ibatis/sql-map-config.xml");
        }
        catch (IOException e) {
            System.out.println(e.toString());
        }
        sqlMapClient = SqlMapClientBuilder.buildSqlMapClient(reader);
    }

    public static boolean IsTagExist(TagDO tagDO) throws SQLException {
        List<TagDO> tagDOList = sqlMapClient.queryForList("GetTagsByDataObject", tagDO);
        return !tagDOList.isEmpty();
    }

    public static ResultOfDataAccess BindTagByDataObject(TagDO newTagDO) throws SQLException {
        ResultOfDataAccess resultOfBindTag = new ResultOfDataAccess();
        resultOfBindTag.setCommandInRequest(Command.BindTagByDataObject);
        if(!IsTagExist(newTagDO)) {
            Long newTagId = (Long) sqlMapClient.insert("BindTagByDataObject", newTagDO);
            if (newTagId != null) {
                resultOfBindTag.setSuccess(true);
                resultOfBindTag.setResult(newTagId);
            } else {
                resultOfBindTag.setSuccess(false);
                resultOfBindTag.setResult(null);
            }
        } else {
            resultOfBindTag.setSuccess(false);
            resultOfBindTag.setResult("Tag has existed!");
        }
        return resultOfBindTag;
    }

    public static ResultOfDataAccess UnBindTagByDataObject(TagDO tagDO) throws SQLException {
        ResultOfDataAccess resultOfUnBindTag = new ResultOfDataAccess();
        resultOfUnBindTag.setCommandInRequest(Command.UnBindTagByDataObject);
        int affectedRowNum = sqlMapClient.delete("UnBindTagByDataObject", tagDO);
        if(affectedRowNum == 1) {
            resultOfUnBindTag.setSuccess(true);
            resultOfUnBindTag.setResult(affectedRowNum);
        }
        else {
            resultOfUnBindTag.setSuccess(false);
            resultOfUnBindTag.setResult(null);
        }
        return resultOfUnBindTag;
    }

    public static ResultOfDataAccess GetTagsByPsyUId(Long psyUIid) throws SQLException {
        ResultOfDataAccess resultOfGetTags = new ResultOfDataAccess();
        resultOfGetTags.setCommandInRequest(Command.GetTagsByPsyUId);
        List<TagDO> tagDOList = sqlMapClient.queryForList("GetTagsByPsyUId", psyUIid);
        resultOfGetTags.setSuccess(true);
        resultOfGetTags.setResult(tagDOList);
        return resultOfGetTags;
    }
}