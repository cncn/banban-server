package com.banban.server.dataaccess.object;

import com.banban.common.ResultOfDataAccess;
import com.banban.common.dataobjects.DealOrderDO;
import com.banban.common.dataobjects.DemandOrderDO;
import com.banban.common.dataobjects.PsychologistDO;
import com.banban.common.dataobjects.UserDO;
import com.banban.common.request.Command;
import com.banban.server.easemob.EasemobMessages;
import com.google.gson.Gson;
import com.ibatis.common.resources.Resources;
import com.ibatis.sqlmap.client.SqlMapClient;
import com.ibatis.sqlmap.client.SqlMapClientBuilder;

import java.io.IOException;
import java.io.Reader;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2015/2/12.
 */
public class DealOrderDAO {

    private static SqlMapClient sqlMapClient = null;

    static {
        Reader reader = null;
        try {
            reader = Resources.getResourceAsReader("ibatis/sql-map-config.xml");
        }
        catch (IOException e) {
            System.out.println(e.toString());
        }
        sqlMapClient = SqlMapClientBuilder.buildSqlMapClient(reader);
    }

    public static boolean IsDemandOidProcessed(Long demandOid) throws SQLException {
        List<DealOrderDO> dealOrderDOList = sqlMapClient.queryForList("GetDealOrdersByDemandOid", demandOid);
        return !dealOrderDOList.isEmpty();
    }

    public static ResultOfDataAccess AcceptDealOrderThroughDemandOrder(DealOrderDO newDealOrderDO) throws Exception {
        //TODO 此处以后需校验newDealOrderDO中信息是否与需求单一致
        ResultOfDataAccess resultOfAcceptDealOrder = new ResultOfDataAccess();
        resultOfAcceptDealOrder.setCommandInRequest(Command.AcceptDealOrderThroughDemandOrder);
        if(DemandOrderDAO.GetDemandOrderById(newDealOrderDO.getDemandOid()) != null) {
            if(!IsDemandOidProcessed(newDealOrderDO.getDemandOid())) {
                Long newDealOrderId = (Long) sqlMapClient.insert("InsertDealOrderByDataObject", newDealOrderDO);
                if (newDealOrderId != null) {
                    DemandOrderDO demandOrderDO = new DemandOrderDO();
                    demandOrderDO.setId(newDealOrderDO.getDemandOid());
                    demandOrderDO.setStatus("Y");
                    DemandOrderDAO.UpdateDemandOrderByDataObject(demandOrderDO);

                    ResultOfDataAccess resultOfGetPsy = PsychologistDAO.GetRecommendedPsychologists();
                    List<PsychologistDO> psychologistDOList = (List<PsychologistDO>) resultOfGetPsy.getResult();
                    List<String> easemobIds = new ArrayList<>();
                    for(PsychologistDO psychologistDO : psychologistDOList) {
                        if(!psychologistDO.getId().equals(newDealOrderDO.getPsyUid())) easemobIds.add(psychologistDO.getEasemobId());
                    }
                    DemandOrderDO cmdObject = new DemandOrderDO();
                    cmdObject.setId(newDealOrderDO.getDemandOid());
                    cmdObject.setStatus("Y");
                    EasemobMessages.SendCmdToUsers(easemobIds, new Gson().toJson(cmdObject));

                    PsychologistDO psychologistDO = PsychologistDAO.GetPsychologistByIdAsFriend(newDealOrderDO.getPsyUid());
                    UserDO userDO = UserDAO.GetUserByIdAsFriend(newDealOrderDO.getUserId());
                    easemobIds.clear();
                    easemobIds.add(userDO.getEasemobId());
                    EasemobMessages.SendCmdToUsers(easemobIds, new Gson().toJson(psychologistDO));
                    resultOfAcceptDealOrder.setSuccess(true);
                    resultOfAcceptDealOrder.setResult(newDealOrderId);
                } else {
                    resultOfAcceptDealOrder.setSuccess(false);
                    resultOfAcceptDealOrder.setResult(null);
                }
            } else {
                resultOfAcceptDealOrder.setSuccess(false);
                resultOfAcceptDealOrder.setResult("The demand order has been processed!");
            }
        } else {
            resultOfAcceptDealOrder.setSuccess(false);
            resultOfAcceptDealOrder.setResult("The demand order is not exist!");
        }
        return resultOfAcceptDealOrder;
    }

    public static ResultOfDataAccess UpdateDealOrderByDataObject(DealOrderDO dealOrderDO) throws SQLException {
        ResultOfDataAccess resultOfUpdateDealOrder = new ResultOfDataAccess();
        resultOfUpdateDealOrder.setCommandInRequest(Command.UpdateDealOrderByDataObject);
        int affectedRowNum = sqlMapClient.update("UpdateDealOrderByDataObject", dealOrderDO);
        if (affectedRowNum == 1) {
            resultOfUpdateDealOrder.setSuccess(true);
            resultOfUpdateDealOrder.setResult(affectedRowNum);
        } else {
            resultOfUpdateDealOrder.setSuccess(false);
            resultOfUpdateDealOrder.setResult(null);
        }
        return resultOfUpdateDealOrder;
    }

    public static ResultOfDataAccess GetDealOrdersByUserId(Long userId) throws SQLException {
        ResultOfDataAccess resultOfGetDealOrders = new ResultOfDataAccess();
        resultOfGetDealOrders.setCommandInRequest(Command.GetDealOrdersByUserId);
        List<DealOrderDO> dealOrderDOList = sqlMapClient.queryForList("GetDealOrdersByUserId", userId);
        resultOfGetDealOrders.setSuccess(true);
        resultOfGetDealOrders.setResult(dealOrderDOList);
        return resultOfGetDealOrders;
    }

    public static ResultOfDataAccess GetDealOrdersByPsyUId(Long psyUid) throws SQLException {
        ResultOfDataAccess resultOfGetDealOrders = new ResultOfDataAccess();
        resultOfGetDealOrders.setCommandInRequest(Command.GetDealOrdersByPsyUId);
        List<DealOrderDO> dealOrderDOList = sqlMapClient.queryForList("GetDealOrdersByPsyUId", psyUid);
        resultOfGetDealOrders.setSuccess(true);
        resultOfGetDealOrders.setResult(dealOrderDOList);
        return resultOfGetDealOrders;
    }
}