package com.banban.server.dataaccess.object;

import com.banban.common.ResultOfDataAccess;
import com.banban.common.dataobjects.DemandOrderDO;
import com.banban.common.dataobjects.PsychologistDO;
import com.banban.common.request.Command;
import com.banban.server.easemob.EasemobMessages;
import com.google.gson.Gson;
import com.ibatis.common.resources.Resources;
import com.ibatis.sqlmap.client.SqlMapClient;
import com.ibatis.sqlmap.client.SqlMapClientBuilder;

import java.io.IOException;
import java.io.Reader;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Administrator on 2015/2/8.
 */
public class DemandOrderDAO {

    private static SqlMapClient sqlMapClient = null;
    private static HashMap<Long, Boolean> currentDemandOrders = new HashMap<>();

    static {
        Reader reader = null;
        try {
            reader = Resources.getResourceAsReader("ibatis/sql-map-config.xml");
        }
        catch (IOException e) {
            System.out.println(e.toString());
        }
        sqlMapClient = SqlMapClientBuilder.buildSqlMapClient(reader);
    }

    public static DemandOrderDO GetDemandOrderById(Long id) throws SQLException {
        if(id == null) return null;
        DemandOrderDO demandOrderDO = null;
        List<DemandOrderDO> demandOrderDOList = sqlMapClient.queryForList("GetDemandOrderById", id);
        if(demandOrderDOList.size() == 1) demandOrderDO = demandOrderDOList.get(0);
        else if(demandOrderDOList.size() > 1) System.out.println("Data Exception occurs when excuting GetDemandOrderById()!");
        return demandOrderDO;
    }

    public static ResultOfDataAccess PostDemandOrderByDataObject(DemandOrderDO demandOrderDO) throws Exception {
        ResultOfDataAccess resultOfPostDemandOrder = new ResultOfDataAccess();
        resultOfPostDemandOrder.setCommandInRequest(Command.PostDemandOrderByDataObject);
        Long newDemandOrderId = (Long) sqlMapClient.insert("InsertDemandOrderByDataObject", demandOrderDO);
        if(newDemandOrderId != null) {
            ResultOfDataAccess resultOfGetPsy = PsychologistDAO.GetRecommendedPsychologists();
            List<PsychologistDO> psychologistDOList = (List<PsychologistDO>) resultOfGetPsy.getResult();
            List<String> easemobIds = new ArrayList<>();
            for(PsychologistDO psychologistDO : psychologistDOList) easemobIds.add(psychologistDO.getEasemobId());
            demandOrderDO.setId(newDemandOrderId);
            EasemobMessages.SendCmdToUsers(easemobIds, new Gson().toJson(demandOrderDO));
            resultOfPostDemandOrder.setSuccess(true);
            resultOfPostDemandOrder.setResult(newDemandOrderId);
        }
        else {
            resultOfPostDemandOrder.setSuccess(false);
            resultOfPostDemandOrder.setResult(null);
        }
        return resultOfPostDemandOrder;
    }

    public static ResultOfDataAccess CancelDemandOrderById(Long id) throws Exception {
        ResultOfDataAccess resultOfCancelDemandOrder = new ResultOfDataAccess();
        resultOfCancelDemandOrder.setCommandInRequest(Command.CancelDemandOrderById);
        DemandOrderDO demandOrderDO = new DemandOrderDO();
        demandOrderDO.setId(id);
        demandOrderDO.setStatus("C");
        int affectedRowNum = sqlMapClient.update("UpdateDemandOrderByDataObject", demandOrderDO);
        if(affectedRowNum == 1) {
            ResultOfDataAccess resultOfGetPsy = PsychologistDAO.GetRecommendedPsychologists();
            List<PsychologistDO> psychologistDOList = (List<PsychologistDO>) resultOfGetPsy.getResult();
            List<String> easemobIds = new ArrayList<>();
            for(PsychologistDO psychologistDO : psychologistDOList) easemobIds.add(psychologistDO.getEasemobId());
            EasemobMessages.SendCmdToUsers(easemobIds, new Gson().toJson(demandOrderDO));
            resultOfCancelDemandOrder.setSuccess(true);
            resultOfCancelDemandOrder.setResult(affectedRowNum);
        }
        else {
            resultOfCancelDemandOrder.setSuccess(false);
            resultOfCancelDemandOrder.setResult(null);
        }
        return resultOfCancelDemandOrder;
    }

    public static ResultOfDataAccess UpdateDemandOrderByDataObject(DemandOrderDO demandOrderDO) throws Exception {
        ResultOfDataAccess resultOfUpdateDemandOrder = new ResultOfDataAccess();
        resultOfUpdateDemandOrder.setCommandInRequest(Command.UpdateDemandOrderByDataObject);
        int affectedRowNum = sqlMapClient.update("UpdateDemandOrderByDataObject", demandOrderDO);
        if(affectedRowNum == 1) {
            ResultOfDataAccess resultOfGetPsy = PsychologistDAO.GetRecommendedPsychologists();
            List<PsychologistDO> psychologistDOList = (List<PsychologistDO>) resultOfGetPsy.getResult();
            List<String> easemobIds = new ArrayList<>();
            for(PsychologistDO psychologistDO : psychologistDOList) easemobIds.add(psychologistDO.getEasemobId());
            EasemobMessages.SendCmdToUsers(easemobIds, new Gson().toJson(demandOrderDO));
            resultOfUpdateDemandOrder.setSuccess(true);
            resultOfUpdateDemandOrder.setResult(affectedRowNum);
        }
        else {
            resultOfUpdateDemandOrder.setSuccess(false);
            resultOfUpdateDemandOrder.setResult(null);
        }
        return resultOfUpdateDemandOrder;
    }
}