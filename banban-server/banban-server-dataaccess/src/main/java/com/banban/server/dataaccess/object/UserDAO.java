package com.banban.server.dataaccess.object;

import com.banban.common.ResultOfDataAccess;
import com.banban.common.dataobjects.PsychologistDO;
import com.banban.common.dataobjects.RelationshipDO;
import com.banban.common.dataobjects.UserDO;
import com.banban.common.request.Command;
import com.banban.server.easemob.EasemobIMUsers;
import com.ibatis.common.resources.Resources;
import com.ibatis.sqlmap.client.SqlMapClient;
import com.ibatis.sqlmap.client.SqlMapClientBuilder;

import java.io.IOException;
import java.io.Reader;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Administrator on 2014/12/5.
 */
public class UserDAO {

    private static SqlMapClient sqlMapClient = null;

    static {
        Reader reader = null;
        try {
            reader = Resources.getResourceAsReader("ibatis/sql-map-config.xml");
        }
        catch (IOException e) {
            System.out.println(e.toString());
        }
        sqlMapClient = SqlMapClientBuilder.buildSqlMapClient(reader);
    }

    public static UserDO GetUserByIdAsStranger(Long id) throws SQLException {
        UserDO userDOAsStranger = null;
        List<UserDO> list = sqlMapClient.queryForList("GetUserById", id);
        if(list.size() == 1) {
            UserDO userDO = list.get(0);
            userDOAsStranger = new UserDO(userDO, UserDO.ConstructorType.CopyPublicProperties);
        }
        return userDOAsStranger;
    }

    public static UserDO GetUserByIdAsFriend(Long id) throws SQLException {
        UserDO userDOAsFriend = null;
        List<UserDO> list = sqlMapClient.queryForList("GetUserById", id);
        if(list.size() == 1) {
            UserDO userDO = list.get(0);
            userDOAsFriend = new UserDO(userDO, UserDO.ConstructorType.CopyPublicPropertiesForFriend);
        }
        return userDOAsFriend;
    }

    public static boolean IsUserExistByLoginId(String loginId) throws SQLException {
        boolean isUserExist = false;
        List<UserDO> userDOList = sqlMapClient.queryForList("IsUserExistByLoginId", loginId);
        if(userDOList.size() == 1) isUserExist = true;
        else if(userDOList.size() > 1) System.out.println("Error: There are data exception in database -- multiple same " +
                "login_id '" + loginId + "' are stored in table 'user', when excuting function IsUserExistByLoginId().");
        return isUserExist;
    }

    public static ResultOfDataAccess RegisterUserByDataObject(UserDO newUserDO) throws SQLException {
        ResultOfDataAccess resultOfRegisterUser = new ResultOfDataAccess();
        resultOfRegisterUser.setCommandInRequest(Command.RegisterUserByDataObject);
        newUserDO.setEasemobId("em_" + newUserDO.getLoginId());
        Long newId = (Long) sqlMapClient.insert("RegisterUserByDataObject", newUserDO);
        if(newId != null) {
            EasemobIMUsers.CreateNewEasemobUserSingle(newUserDO.getEasemobId(), "123456");
            resultOfRegisterUser.setSuccess(true);
            resultOfRegisterUser.setResult(newId);
        }
        else {
            resultOfRegisterUser.setSuccess(false);
            resultOfRegisterUser.setResult(null);
        }
        return resultOfRegisterUser;
    }

    public static ResultOfDataAccess DisableUserByLoginIdAndPassword(HashMap<String, String> loginIdAndPassword) throws SQLException {
        ResultOfDataAccess resultOfDisableUser = new ResultOfDataAccess();
        resultOfDisableUser.setCommandInRequest(Command.DisableUserByLoginIdAndPassword);
        int affectedRowNum = sqlMapClient.update("DisableUserByLoginIdAndPassword", loginIdAndPassword);
        if(affectedRowNum == 1) {
            resultOfDisableUser.setSuccess(true);
            resultOfDisableUser.setResult(affectedRowNum);
        }
        else {
            resultOfDisableUser.setSuccess(false);
            resultOfDisableUser.setResult("Warn: The loginId or password may be wrong!");
        }
        return resultOfDisableUser;
    }

    public static ResultOfDataAccess UpdateUserByDataObject(UserDO newUserDO) throws SQLException {
        ResultOfDataAccess resultOfUpdateUser = new ResultOfDataAccess();
        resultOfUpdateUser.setCommandInRequest(Command.UpdateUserByDataObject);
        int affectedRowNum = sqlMapClient.update("UpdateUserByDataObject", newUserDO);
        if(affectedRowNum == 1) {
            resultOfUpdateUser.setSuccess(true);
            resultOfUpdateUser.setResult(affectedRowNum);
        }
        else {
            resultOfUpdateUser.setSuccess(false);
            resultOfUpdateUser.setResult(null);
        }
        return resultOfUpdateUser;
    }

    public static ResultOfDataAccess GetUserByLoginIdAndPassword(HashMap<String, String> loginIdAndPassword) throws SQLException {
        ResultOfDataAccess resultOfGetUser = new ResultOfDataAccess();
        resultOfGetUser.setCommandInRequest(Command.GetUserByLoginIdAndPassword);
        List<UserDO> userDOList = sqlMapClient.queryForList("GetUserByLoginIdAndPassword", loginIdAndPassword);
        if(userDOList.size() == 1) {
            UserDO userDO = userDOList.get(0);
            resultOfGetUser.setSuccess(true);
            List<RelationshipDO> relationshipDOList = RelationshipDAO.GetRelationshipByUserId(userDO.getId());
            if (relationshipDOList != null) {
                List<PsychologistDO> friends = new ArrayList<>();
                for (RelationshipDO relationshipDO : relationshipDOList) {
                    PsychologistDO psychologistDOAsFriend = PsychologistDAO.GetPsychologistByIdAsFriend(relationshipDO.getPsyUid());
                    if(psychologistDOAsFriend != null) friends.add(psychologistDOAsFriend);
                }
                userDO.setPsyFriends(friends);
            }
            resultOfGetUser.setResult(userDO);
        }
        else if(userDOList.size() == 0){
            resultOfGetUser.setSuccess(false);
            resultOfGetUser.setResult("Warn: The loginId or password may be wrong!");
        }
        else {
            resultOfGetUser.setSuccess(false);
            resultOfGetUser.setResult(null);
            System.out.println("Error: There are data exception in database -- multiple same login_id '"
                    + loginIdAndPassword.get("loginId") +
                    "' are stored in table 'user', when excuting function GetUserByLoginIdAndPassword().");
        }
        return resultOfGetUser;
    }

    public static ResultOfDataAccess VisitUserById(Long id) throws SQLException {
        ResultOfDataAccess resultOfVisitUser = new ResultOfDataAccess();
        resultOfVisitUser.setCommandInRequest(Command.VisitUserById);
        UserDO userDO;
        boolean isFriend = true;
        if(isFriend) userDO = GetUserByIdAsFriend(id);
        else userDO = GetUserByIdAsStranger(id);
        if(userDO != null) {
            resultOfVisitUser.setSuccess(true);
            resultOfVisitUser.setResult(userDO);
        }
        else {
            resultOfVisitUser.setSuccess(false);
            resultOfVisitUser.setResult(null);
        }
        return resultOfVisitUser;
    }
}