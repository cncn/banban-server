package com.banban.server.dataaccess.object;

import com.banban.common.ResultOfDataAccess;
import com.banban.common.dataobjects.PsychologistDO;
import com.banban.common.dataobjects.RelationshipDO;
import com.banban.common.dataobjects.UserDO;
import com.banban.common.request.Command;
import com.ibatis.common.resources.Resources;
import com.ibatis.sqlmap.client.SqlMapClient;
import com.ibatis.sqlmap.client.SqlMapClientBuilder;

import java.io.IOException;
import java.io.Reader;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Administrator on 2015/1/25.
 */
public class RelationshipDAO {

    private static SqlMapClient sqlMapClient = null;

    static {
        Reader reader = null;
        try {
            reader = Resources.getResourceAsReader("ibatis/sql-map-config.xml");
        }
        catch (IOException e) {
            System.out.println(e.toString());
        }
        sqlMapClient = SqlMapClientBuilder.buildSqlMapClient(reader);
    }

    public static RelationshipDO GetRelationshipByUserIdAndPsyUid(HashMap<String, Long> UserIdAndPsyUid) throws SQLException {
        RelationshipDO relationshipDO = null;
        List<RelationshipDO> relationshipDOList = sqlMapClient.queryForList("GetRelationshipByUserIdAndPsyUid", UserIdAndPsyUid);
        if(relationshipDOList.size() == 1) relationshipDO = relationshipDOList.get(0);
        return relationshipDO;
    }

    public static List<RelationshipDO> GetRelationshipByUserId(Long userId) throws SQLException {
        return sqlMapClient.queryForList("GetRelationshipByUserId", userId);
    }

    public static List<RelationshipDO> GetRelationshipByPsyUid(Long psyUid) throws SQLException {
        return sqlMapClient.queryForList("GetRelationshipByPsyUid", psyUid);
    }

    public static ResultOfDataAccess RegisterRelationshipByDataObject(RelationshipDO newRelationshipDO) throws SQLException {
        ResultOfDataAccess resultOfRegisterRelationship = new ResultOfDataAccess();
        resultOfRegisterRelationship.setCommandInRequest(Command.RegisterRelationshipByDataObject);
        HashMap<String, Long> userIdAndPsyUid = new HashMap<>();
        userIdAndPsyUid.put("userId", newRelationshipDO.getUserId());
        userIdAndPsyUid.put("psyUid", newRelationshipDO.getPsyUid());
        if(GetRelationshipByUserIdAndPsyUid(userIdAndPsyUid) == null) {
            Long newRelationshipId = (Long) sqlMapClient.insert("RegisterRelationshipByDataObject", newRelationshipDO);
            if (newRelationshipId != null) {
                resultOfRegisterRelationship.setSuccess(true);
                resultOfRegisterRelationship.setResult(newRelationshipId);
            } else {
                resultOfRegisterRelationship.setSuccess(false);
                resultOfRegisterRelationship.setResult(null);
            }
        } else {
            resultOfRegisterRelationship.setSuccess(false);
            resultOfRegisterRelationship.setResult("The relationship has existed!");
        }
        return resultOfRegisterRelationship;
    }

    public static ResultOfDataAccess UpdateRelationshipByDataObject(RelationshipDO relationshipDO) throws SQLException {
        ResultOfDataAccess resultOfUpdateRelationship = new ResultOfDataAccess();
        resultOfUpdateRelationship.setCommandInRequest(Command.UpdateRelationshipByDataObject);
        int affectedRowNum = sqlMapClient.update("UpdateRelationshipByDataObject", relationshipDO);
        if (affectedRowNum == 1) {
            resultOfUpdateRelationship.setSuccess(true);
            resultOfUpdateRelationship.setResult(affectedRowNum);
        } else {
            resultOfUpdateRelationship.setSuccess(false);
            resultOfUpdateRelationship.setResult("The relationship may be not existed!");
        }
        return resultOfUpdateRelationship;
    }

    public static ResultOfDataAccess GetAllFriendsByUserId(Long userId) throws SQLException {
        ResultOfDataAccess resultOfGetAllFriends = new ResultOfDataAccess();
        resultOfGetAllFriends.setCommandInRequest(Command.GetAllFriendsByUserId);
        List<RelationshipDO> relationshipDOList = GetRelationshipByUserId(userId);
        if (relationshipDOList != null) {
            List<PsychologistDO> friends = new ArrayList<>();
            for (RelationshipDO relationshipDO : relationshipDOList) {
                PsychologistDO psychologistDOAsFriend = PsychologistDAO.GetPsychologistByIdAsFriend(relationshipDO.getPsyUid());
                if(psychologistDOAsFriend != null) friends.add(psychologistDOAsFriend);
            }
            resultOfGetAllFriends.setSuccess(true);
            resultOfGetAllFriends.setResult(friends);
        }
        else {
            resultOfGetAllFriends.setSuccess(false);
            resultOfGetAllFriends.setResult(null);
        }
        return resultOfGetAllFriends;
    }

    public static ResultOfDataAccess GetAllFriendsByPsyUid(Long psyUid) throws SQLException {
        ResultOfDataAccess resultOfGetAllFriends = new ResultOfDataAccess();
        resultOfGetAllFriends.setCommandInRequest(Command.GetAllFriendsByPsyUid);
        List<RelationshipDO> relationshipDOList = GetRelationshipByPsyUid(psyUid);
        if (relationshipDOList != null) {
            List<UserDO> friends = new ArrayList<>();
            for (RelationshipDO relationshipDO : relationshipDOList) {
                UserDO userDOAsFriend = UserDAO.GetUserByIdAsFriend(relationshipDO.getUserId());
                if(userDOAsFriend != null) friends.add(userDOAsFriend);
            }
            resultOfGetAllFriends.setSuccess(true);
            resultOfGetAllFriends.setResult(friends);
        }
        else {
            resultOfGetAllFriends.setSuccess(false);
            resultOfGetAllFriends.setResult(null);
        }
        return resultOfGetAllFriends;
    }
}