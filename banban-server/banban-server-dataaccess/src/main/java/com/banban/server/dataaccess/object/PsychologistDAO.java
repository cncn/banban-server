package com.banban.server.dataaccess.object;

import com.banban.common.ResultOfDataAccess;
import com.banban.common.dataobjects.PsychologistDO;
import com.banban.common.dataobjects.RelationshipDO;
import com.banban.common.dataobjects.TagDO;
import com.banban.common.dataobjects.UserDO;
import com.banban.common.request.Command;
import com.banban.server.easemob.EasemobIMUsers;
import com.ibatis.common.resources.Resources;
import com.ibatis.sqlmap.client.SqlMapClient;
import com.ibatis.sqlmap.client.SqlMapClientBuilder;

import java.io.IOException;
import java.io.Reader;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by root on 15-2-5.
 */
public class PsychologistDAO {

    private static SqlMapClient sqlMapClient = null;

    static {
        Reader reader = null;
        try {
            reader = Resources.getResourceAsReader("ibatis/sql-map-config.xml");
        }
        catch (IOException e) {
            System.out.println(e.toString());
        }
        sqlMapClient = SqlMapClientBuilder.buildSqlMapClient(reader);
    }

    public static PsychologistDO GetPsychologistByIdAsStranger(Long id) throws SQLException {
        PsychologistDO psychologistDOAsStranger = null;
        List<PsychologistDO> psychologistDOList = sqlMapClient.queryForList("GetPsychologistById", id);
        if(psychologistDOList.size() == 1) {
            PsychologistDO psychologistDO = psychologistDOList.get(0);
            psychologistDOAsStranger = new PsychologistDO(psychologistDO, PsychologistDO.ConstructorType.CopyPublicProperties);
        }
        return psychologistDOAsStranger;
    }

    public static PsychologistDO GetPsychologistByIdAsFriend(Long id) throws SQLException {
        PsychologistDO psychologistDOAsFriend = null;
        List<PsychologistDO> psychologistDOList = sqlMapClient.queryForList("GetPsychologistById", id);
        if(psychologistDOList.size() == 1) {
            PsychologistDO psychologistDO = psychologistDOList.get(0);
            psychologistDOAsFriend = new PsychologistDO(psychologistDO, PsychologistDO.ConstructorType.CopyPublicPropertiesForFriend);
        }
        return psychologistDOAsFriend;
    }

    public static ResultOfDataAccess RegisterPsychologistByDataObject(PsychologistDO newPsychologistDO) throws SQLException {
        ResultOfDataAccess resultOfRegisterPsychologist = new ResultOfDataAccess();
        resultOfRegisterPsychologist.setCommandInRequest(Command.RegisterPsychologistByDataObject);
        newPsychologistDO.setEasemobId("em_" + newPsychologistDO.getLoginId());
        Long newPsyId = (Long) sqlMapClient.insert("RegisterPsychologistByDataObject", newPsychologistDO);
        if(newPsyId != null) {
            EasemobIMUsers.CreateNewEasemobUserSingle(newPsychologistDO.getEasemobId(), "123456");
            newPsychologistDO.setPsyId(newPsyId);
            ResultOfDataAccess resultOfRegisterUser = UserDAO.RegisterUserByDataObject(newPsychologistDO);
            if(resultOfRegisterUser.isSuccess()) {
                resultOfRegisterPsychologist.setSuccess(true);
                resultOfRegisterPsychologist.setResult(resultOfRegisterUser.getResult());
            }
            else {
                sqlMapClient.delete("DeletePsychologistById", newPsyId);
                resultOfRegisterPsychologist.setSuccess(false);
                resultOfRegisterPsychologist.setResult(null);
            }
        }
        else {
            resultOfRegisterPsychologist.setSuccess(false);
            resultOfRegisterPsychologist.setResult(null);
        }
        return resultOfRegisterPsychologist;
    }

    public static ResultOfDataAccess DisablePsychologistByLoginIdAndPassword(HashMap<String, String> loginIdAndPassword) throws SQLException {
        ResultOfDataAccess resultOfDisablePsychologist = new ResultOfDataAccess();
        resultOfDisablePsychologist.setCommandInRequest(Command.DisablePsychologistByLoginIdAndPassword);
        ResultOfDataAccess resultOfGetUser = UserDAO.GetUserByLoginIdAndPassword(loginIdAndPassword);
        if(resultOfGetUser.isSuccess()) {
            UserDO userDO = (UserDO) resultOfGetUser.getResult();
            if(userDO.getPsyId() != null) {
                int affectedRowNum = sqlMapClient.update("DisablePsychologistByLoginIdAndPassword", userDO.getPsyId());
                if (affectedRowNum == 1) {
                    resultOfDisablePsychologist.setSuccess(true);
                    resultOfDisablePsychologist.setResult(affectedRowNum);
                } else {
                    resultOfDisablePsychologist.setSuccess(false);
                    resultOfDisablePsychologist.setResult("Warn: The user may not be psychologist!");
                }
            } else {
                resultOfDisablePsychologist.setSuccess(false);
                resultOfDisablePsychologist.setResult("Warn: The user isn't psychologist!");
            }
        } else {
            resultOfDisablePsychologist.setSuccess(false);
            resultOfDisablePsychologist.setResult("Warn: The loginId or password may be wrong!");
        }
        return resultOfDisablePsychologist;
    }

    public static ResultOfDataAccess UpdatePsychologistByDataObject(PsychologistDO newPsychologistDO) throws SQLException {
        ResultOfDataAccess resultOfUpdatePsychologist = new ResultOfDataAccess();
        resultOfUpdatePsychologist.setCommandInRequest(Command.UpdatePsychologistByDataObject);
        int affectedRowNum = sqlMapClient.update("UpdatePsychologistByDataObject", newPsychologistDO);
        if(affectedRowNum == 1) {
            resultOfUpdatePsychologist.setSuccess(true);
            resultOfUpdatePsychologist.setResult(affectedRowNum);
        }
        else {
            resultOfUpdatePsychologist.setSuccess(false);
            resultOfUpdatePsychologist.setResult(null);
        }
        return resultOfUpdatePsychologist;
    }

    public static ResultOfDataAccess GetPsychologistByLoginIdAndPassword(HashMap<String, String> loginIdAndPassword) throws SQLException {
        ResultOfDataAccess resultOfGetPsychologist = new ResultOfDataAccess();
        resultOfGetPsychologist.setCommandInRequest(Command.GetPsychologistByLoginIdAndPassword);
        List<PsychologistDO> PsychologistDOList = sqlMapClient.queryForList("GetPsychologistByLoginIdAndPassword", loginIdAndPassword);
        if(PsychologistDOList.size() == 1) {
            PsychologistDO psychologistDO = PsychologistDOList.get(0);
            resultOfGetPsychologist.setSuccess(true);

            List<RelationshipDO> relationshipDOList = RelationshipDAO.GetRelationshipByPsyUid(psychologistDO.getId());
            if (relationshipDOList != null) {
                List<UserDO> friends = new ArrayList<>();
                for (RelationshipDO relationshipDO : relationshipDOList) {
                    UserDO userDOAsFriend = UserDAO.GetUserByIdAsFriend(relationshipDO.getUserId());
                    if(userDOAsFriend != null) friends.add(userDOAsFriend);
                }
                psychologistDO.setUserFriends(friends);
            }

            ResultOfDataAccess resultOfGetTags = TagDAO.GetTagsByPsyUId(psychologistDO.getId());
            if(resultOfGetTags.isSuccess()) {
                List<TagDO> tagDOList = (List<TagDO>) resultOfGetTags.getResult();
                psychologistDO.setTags(tagDOList);
            }

            resultOfGetPsychologist.setResult(psychologistDO);
        }
        else if(PsychologistDOList.size() == 0){
            resultOfGetPsychologist.setSuccess(false);
            resultOfGetPsychologist.setResult("Warn: The loginId or password may be wrong!");
        }
        else {
            resultOfGetPsychologist.setSuccess(false);
            resultOfGetPsychologist.setResult(null);
            System.out.println("Error: There are data exception in database -- multiple same login_id '"
                    + loginIdAndPassword.get("loginId") +
                    "' are stored in table 'user', when excuting function GetPsychologistByLoginIdAndPassword().");
        }
        return resultOfGetPsychologist;
    }

    public static ResultOfDataAccess VisitPsychologistById(Long id) throws SQLException {
        ResultOfDataAccess resultOfVisitPsychologist = new ResultOfDataAccess();
        resultOfVisitPsychologist.setCommandInRequest(Command.VisitPsychologistById);
        PsychologistDO psychologistDO;
        boolean isFriend = true;
        if(isFriend) psychologistDO = GetPsychologistByIdAsFriend(id);
        else psychologistDO = GetPsychologistByIdAsStranger(id);
        if(psychologistDO != null) {
            resultOfVisitPsychologist.setSuccess(true);
            resultOfVisitPsychologist.setResult(psychologistDO);
        }
        else {
            resultOfVisitPsychologist.setSuccess(false);
            resultOfVisitPsychologist.setResult(null);
        }
        return resultOfVisitPsychologist;
    }

    public static ResultOfDataAccess GetRecommendedPsychologists() throws SQLException {
        ResultOfDataAccess resultOfDataAccess = new ResultOfDataAccess();
        resultOfDataAccess.setCommandInRequest(Command.GetRecommendedPsychologists);
        List<PsychologistDO> psyList = sqlMapClient.queryForList("GetRecommendedPsychologists", null);
        List<PsychologistDO> psyAsStrangerList = new ArrayList<>();
        for(PsychologistDO psychologistDO : psyList) {
            PsychologistDO psychologistDOAsStranger = new PsychologistDO(psychologistDO, PsychologistDO.ConstructorType.CopyPublicProperties);
            psyAsStrangerList.add(psychologistDOAsStranger);
        }
        resultOfDataAccess.setSuccess(true);
        resultOfDataAccess.setResult(psyAsStrangerList);
        return resultOfDataAccess;
    }
}