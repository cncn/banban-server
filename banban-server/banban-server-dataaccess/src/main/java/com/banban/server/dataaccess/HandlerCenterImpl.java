package com.banban.server.dataaccess;

import com.banban.common.ResultOfDataAccess;
import com.banban.common.dataobjects.*;
import com.banban.common.request.Command;
import com.banban.server.common.HandlerCenter;
import com.banban.server.dataaccess.object.*;
import com.google.gson.Gson;

import java.util.HashMap;

/**
 * Created by Administrator on 2014/12/4.
 */
public class HandlerCenterImpl implements HandlerCenter {

    @Override
    public ResultOfDataAccess excute(int command, String arguments) throws Exception {
        ResultOfDataAccess resultOfDataAccess;
        if(command == Command.RegisterUserByDataObject) {
            UserDO userDO = new Gson().fromJson(arguments, UserDO.class);
            resultOfDataAccess = UserDAO.RegisterUserByDataObject(userDO);
        } else if(command == Command.DisableUserByLoginIdAndPassword) {
            HashMap<String, String> parameters = new Gson().fromJson(arguments, HashMap.class);
            resultOfDataAccess = UserDAO.DisableUserByLoginIdAndPassword(parameters);
        } else if(command == Command.UpdateUserByDataObject) {
            UserDO userDO = new Gson().fromJson(arguments, UserDO.class);
            resultOfDataAccess = UserDAO.UpdateUserByDataObject(userDO);
        } else if(command == Command.GetUserByLoginIdAndPassword) {
            HashMap<String, String> parameters = new Gson().fromJson(arguments, HashMap.class);
            resultOfDataAccess = UserDAO.GetUserByLoginIdAndPassword(parameters);
        } else if(command == Command.VisitUserById) {
            Long userId = new Gson().fromJson(arguments, Long.class);
            resultOfDataAccess = UserDAO.VisitUserById(userId);
        }

        else if(command == Command.RegisterPsychologistByDataObject) {
            PsychologistDO psychologistDO = new Gson().fromJson(arguments, PsychologistDO.class);
            resultOfDataAccess = PsychologistDAO.RegisterPsychologistByDataObject(psychologistDO);
        } else if(command == Command.DisablePsychologistByLoginIdAndPassword) {
            HashMap<String, String> parameters = new Gson().fromJson(arguments, HashMap.class);
            resultOfDataAccess = PsychologistDAO.DisablePsychologistByLoginIdAndPassword(parameters);
        } else if(command == Command.UpdatePsychologistByDataObject) {
            PsychologistDO psychologistDO = new Gson().fromJson(arguments, PsychologistDO.class);
            resultOfDataAccess = PsychologistDAO.UpdatePsychologistByDataObject(psychologistDO);
        } else if(command == Command.GetPsychologistByLoginIdAndPassword) {
            HashMap<String, String> parameters = new Gson().fromJson(arguments, HashMap.class);
            resultOfDataAccess = PsychologistDAO.GetPsychologistByLoginIdAndPassword(parameters);
        } else if(command == Command.VisitPsychologistById) {
            Long psychologistId = new Gson().fromJson(arguments, Long.class);
            resultOfDataAccess = PsychologistDAO.VisitPsychologistById(psychologistId);
        } else if(command == Command.GetRecommendedPsychologists) {
            resultOfDataAccess = PsychologistDAO.GetRecommendedPsychologists();
        }

        else if(command == Command.RegisterRelationshipByDataObject) {
            RelationshipDO newRelationshipDO = new Gson().fromJson(arguments, RelationshipDO.class);
            resultOfDataAccess = RelationshipDAO.RegisterRelationshipByDataObject(newRelationshipDO);
        } else if(command == Command.UpdateRelationshipByDataObject) {
            RelationshipDO relationshipDO = new Gson().fromJson(arguments, RelationshipDO.class);
            resultOfDataAccess = RelationshipDAO.UpdateRelationshipByDataObject(relationshipDO);
        } else if(command == Command.GetAllFriendsByUserId) {
            Long userId = new Gson().fromJson(arguments, Long.class);
            resultOfDataAccess = RelationshipDAO.GetAllFriendsByUserId(userId);
        } else if(command == Command.GetAllFriendsByPsyUid) {
            Long psychologistId = new Gson().fromJson(arguments, Long.class);
            resultOfDataAccess = RelationshipDAO.GetAllFriendsByPsyUid(psychologistId);
        }

        else if(command == Command.GetPhotosByUserId) {
            Long userId = new Gson().fromJson(arguments, Long.class);
            resultOfDataAccess = PhotoDAO.GetPhotosByUserId(userId);
        }

        else if(command == Command.BindTagByDataObject) {
            TagDO newTagDO = new Gson().fromJson(arguments, TagDO.class);
            resultOfDataAccess = TagDAO.BindTagByDataObject(newTagDO);
        } else if(command == Command.UnBindTagByDataObject) {
            TagDO tagDO = new Gson().fromJson(arguments, TagDO.class);
            resultOfDataAccess = TagDAO.UnBindTagByDataObject(tagDO);
        } else if(command == Command.GetTagsByPsyUId) {
            Long psyUid = new Gson().fromJson(arguments, Long.class);
            resultOfDataAccess = TagDAO.GetTagsByPsyUId(psyUid);
        }

        else if(command == Command.PostDemandOrderByDataObject) {
            DemandOrderDO newPostOrderDO = new Gson().fromJson(arguments, DemandOrderDO.class);
            resultOfDataAccess = DemandOrderDAO.PostDemandOrderByDataObject(newPostOrderDO);
        } else if(command == Command.CancelDemandOrderById) {
            Long id = new Gson().fromJson(arguments, Long.class);
            resultOfDataAccess = DemandOrderDAO.CancelDemandOrderById(id);
        } else if(command == Command.UpdateDemandOrderByDataObject) {
            DemandOrderDO postOrderDO = new Gson().fromJson(arguments, DemandOrderDO.class);
            resultOfDataAccess = DemandOrderDAO.UpdateDemandOrderByDataObject(postOrderDO);
        }

        else if(command == Command.AcceptDealOrderThroughDemandOrder) {
            DealOrderDO newDealOrderDO = new Gson().fromJson(arguments, DealOrderDO.class);
            resultOfDataAccess = DealOrderDAO.AcceptDealOrderThroughDemandOrder(newDealOrderDO);
        } else if(command == Command.UpdateDealOrderByDataObject) {
            DealOrderDO dealOrderDO = new Gson().fromJson(arguments, DealOrderDO.class);
            resultOfDataAccess = DealOrderDAO.UpdateDealOrderByDataObject(dealOrderDO);
        } else if(command == Command.GetDealOrdersByUserId) {
            Long userId = new Gson().fromJson(arguments, Long.class);
            resultOfDataAccess = DealOrderDAO.GetDealOrdersByUserId(userId);
        } else if(command == Command.GetDealOrdersByPsyUId) {
            Long psyUid = new Gson().fromJson(arguments, Long.class);
            resultOfDataAccess = DealOrderDAO.GetDealOrdersByPsyUId(psyUid);
        }

        else {
            System.out.println("ERROR: The command -- " + String.valueOf(command) + " is not exist!");
            resultOfDataAccess = new ResultOfDataAccess();
            resultOfDataAccess.setSuccess(false);
            resultOfDataAccess.setCommandInRequest(0);
            resultOfDataAccess.setResult("ERROR: The command -- " + String.valueOf(command) + " is not exist!");
        }
        return resultOfDataAccess;
    }
}