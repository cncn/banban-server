package com.banban.server.easemob;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by Administrator on 2015/2/12.
 */
public class EasemobMessagesTest {

    @Test
    public void getUserStatus() {
        String targetUserPrimaryKey = "em_admin";
        ObjectNode userNode = EasemobMessages.GetUserStatus(targetUserPrimaryKey);
        assertEquals("get", userNode.get("action").asText());
        String userStatus = userNode.get("data").get(targetUserPrimaryKey).asText();
        assertTrue(userStatus.equals("offline") || userStatus.equals("online"));
    }

    @Test
    public void sendMessages() {
        JsonNodeFactory factory = new JsonNodeFactory(false);
        String from = "em_admin";
        String targetTypeus = "users";
        ObjectNode ext = factory.objectNode();
        ArrayNode targetusers = factory.arrayNode();
        String user1 = "em_leonili";
        String user2 = "em_mxyydy";
        targetusers.add(user1);
        targetusers.add(user2);
        ObjectNode txtmsg = factory.objectNode();
        txtmsg.put("msg", "This is message!");
        txtmsg.put("type","txt");
        ObjectNode sendTxtMessageUserNode = EasemobMessages.SendMessages(targetTypeus, targetusers, txtmsg, from, ext);
        assertEquals("success", sendTxtMessageUserNode.get(user1).asText());
        assertEquals("success", sendTxtMessageUserNode.get(user2).asText());
    }

    @Test
    public void IsUserOnline() {
        String targetUserPrimaryKey = "em_admin";
        boolean isUserOnline = EasemobMessages.IsUserOnline(targetUserPrimaryKey);
        assertTrue(isUserOnline || (!isUserOnline));
    }

    @Test
    public void SendCmdToUsers() throws IOException {
        String user1 = "em_leonili";
        String user2 = "em_mxyydy";
        List<String> users = new ArrayList<>();
        users.add(user1);
        users.add(user2);
        String cmd = "{}";
        ObjectNode sendTxtMessageUserNode = EasemobMessages.SendCmdToUsers(users, cmd);
        assertEquals("success", sendTxtMessageUserNode.get(user1).asText());
        assertEquals("success", sendTxtMessageUserNode.get(user2).asText());
    }
}