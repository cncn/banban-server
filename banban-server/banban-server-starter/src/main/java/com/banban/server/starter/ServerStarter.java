package com.banban.server.starter;

import com.banban.server.dataaccess.HandlerCenterImpl;
import com.banban.server.net.http.HttpServer;

/**
 * Created by Administrator on 2014/12/6.
 */
public class ServerStarter {
    public static void main(String[] args) throws Exception {
        HttpServer server = new HttpServer();
        HandlerCenterImpl handlerCenter = new HandlerCenterImpl();
        server.SetHandelCenter(handlerCenter);
        server.start(8000);
    }
}